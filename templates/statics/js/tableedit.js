function tableEditMaxlength(field, length) {
    $('td[data-field="'+field+'"]>input.layui-table-edit').attr('maxlength', length.toString());
}

function tableEditOninput(type, field, callback, maxNum) {
    switch (type) {
        case 'txt':{
           break;
        }
        case 'num':{
            if (!isNotEmpty(maxNum)){
                maxNum = 99999999.99;
            }
            break;
        }
        case 'numInteger':{
            if (!isNotEmpty(maxNum)){
                maxNum = 99999999;
            }
            break;
        }
        default:{
            throw new Error("edit类型有误");
        }
    }
    $('td[data-field="'+field+'"]>input.layui-table-edit').attr('oninput', 'tableEdit(this, \''+type+'\', \''+field+'\', \''+callback+'\', \''+maxNum+'\')');
}

function tableEdit(obj, type, event, callback, maxNum){
    console.log("1:" + obj.value);
    obj.value = obj.value.trim();
    console.log("2:" + obj.value);
    switch (type) {
        case 'txt':{
            break;
        }
        case 'num':{
            obj.value = obj.value.replace(/[^\d.]/g,""); //清除"数字"和"."以外的字符
            console.log("3:" + obj.value);
            obj.value = obj.value.replace(/^\./g,""); //验证第一个字符是数字
            console.log("4:" + obj.value);
            obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个, 清除多余的
            console.log("5:" + obj.value);
            obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
            console.log("6:" + obj.value);
            obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3'); //只能输入两个小数
            console.log("7:" + obj.value);
            break;
        }
        case 'numInteger':{
            obj.value = obj.value.replace(/[^\d]/g,"");
            console.log("3:" + obj.value);
            break;
        }
    }
    if (isNotEmpty(maxNum) && obj.value > new Number(maxNum)){
        obj.value = $(obj)[0].attributes.oldvalue.value;
    }
    console.log("8:" + obj.value);
    $(obj).attr('oldValue', obj.value);
    if (isNotEmpty(callback)) {
        setTimeout(function () {
            eval(callback+'(event, obj.value, $(obj).parent().parent().find(\'td[data-field="serializid"]>div\').text())');
        }, 10);
    }
}

function editMaxlength(field, length) {
    $('input[id="'+field+'"]').attr('maxlength', length.toString());
}

function editOninput(type, field, callback, maxNum) {
    switch (type) {
        case 'txt':{
           break;
        }
        case 'num':{
            if (!isNotEmpty(maxNum)){
                maxNum = 99999999.99;
            }
            break;
        }
        case 'numInteger':{
            if (!isNotEmpty(maxNum)){
                maxNum = 99999999;
            }
            break;
        }
        default:{
            throw new Error("edit类型有误");
        }
    }
    var view = $('input[id="'+field+'"]');
    view.attr({onpaste:"return false",oldValue:view.val()});
    view.attr('oninput', 'edit(this, \''+type+'\', \''+field+'\', \''+callback+'\', \''+maxNum+'\')');
}

function editSimple(obj, type, maxNum){
    edit(obj,type,'','',maxNum, '0');
}

function edit(obj, type, event, callback, maxNum, minNum){
    console.log("1:" + obj.value);
    obj.value = obj.value.trim();
    console.log("2:" + obj.value);
    switch (type) {
        case 'txt':{
            break;
        }
        case 'num':{
            obj.value = obj.value.replace(/[^\d.]/g,""); //清除"数字"和"."以外的字符
            console.log("3:" + obj.value);
            obj.value = obj.value.replace(/^\./g,""); //验证第一个字符是数字
            console.log("4:" + obj.value);
            obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个, 清除多余的
            console.log("5:" + obj.value);
            obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
            console.log("6:" + obj.value);
            obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3'); //只能输入两个小数
            console.log("7:" + obj.value);
            break;
        }
        case 'numInteger':{
            obj.value = obj.value.replace(/[^\d]/g,"");
            console.log("3:" + obj.value);
            break;
        }
    }
    if ((isNotEmpty(maxNum) && obj.value > new Number(maxNum)) || isNotEmpty(minNum) && obj.value < new Number(minNum)){
        if ($(obj)[0].attributes.oldvalue){
            obj.value = $(obj)[0].attributes.oldvalue.value;
        }else {
            obj.value = minNum||0;
        }
    }
    var _val = obj.value;
    console.log("8:" + obj.value);
    $(obj).attr('oldValue', obj.value);
    if (isNotEmpty(callback)) {
        setTimeout(function () {
            eval(callback+'(event, _val)');
        }, 10);
    }
}

/**
 * 列表更多按钮点击无效，是因为点击的不是表格里的元素，因此模拟点击表格里的就好了，声明全局事件处理
 */
function moreButtonsEvent(){
    // 更多弹窗里的按钮
    $(document).on('click','.layui-table-tips-main a',function(){
        var layEvent = $(this).attr("lay-event");
        var moreBtnIndex = $(this).attr("more-btn-index");
        // 模拟点击表格里真正的按钮，触发layer.table的事件
        $(`.laytable-cell-1-0-0 [lay-event=${layEvent}][more-btn-index=${moreBtnIndex}]:eq(0)`).click();
        return false;
    })
}
$(function (){
    moreButtonsEvent()
});