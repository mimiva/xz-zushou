$(function () {
    // 车辆违章预警条件 事件
    layui.form.on('select(illegalConditionConfig)', function (data) {
        vm.controlCenter.illegalConditionConfig = data.value;
    });
    //车辆违章预警条件1,2,3 事件
    layui.form.on('select(illegalConditionItem)', function (data) {
        var illegalConditionItemIndex = data.othis
            .siblings("[name=illegalConditionItemIndex]")
            .val();
        let illegalConditionListElement = vm.controlCenter.illegalConditionList[illegalConditionItemIndex];
        illegalConditionListElement.key = data.value;
        illegalConditionListElement.unit = vm.illegalItemMap[data.value].unit;
    });
    // 财务配置
    layui.form.on('radio(dailyOrderApproval)', function(data){
        vm.controlCenter.dailyOrderApproval = data.value;
    });
    // 自动查询车辆违章-查询车辆单选 1退车，2换车，3用车
    layui.form.on('checkbox(autoQueryVehicleConfig)', function(data){
        vm.controlCenter.autoQueryVehicleConfig = [];
        $("[name=autoQueryVehicleConfig]:checked").each((index, value)=>{
            if(data.elem.checked){
                vm.controlCenter.autoQueryVehicleConfig.push(value.value)
            }
        });
    });
    // 标题开关事件
    layui.form.on('switch(titleOnOff)', function(data){
        vm.controlCenter[data.elem.name] = data.elem.checked?1:0;
    });
    // 车辆违章预警条件开关事件
    layui.form.on('switch(illegalConditionItem)', function(data){
        var illegalConditionItemIndex = data.othis
            .siblings("[name=illegalConditionItemIndex]")
            .val();
        // 响应式更新条件开关
        vm.controlCenter.illegalConditionList[illegalConditionItemIndex].enable = data.elem.checked?1:0;
    });
    layui.form.render();
});

var vm = new Vue({
    el:'#rrapp',
    data: {
        id:null,
        // 租户系统参数配置key，直接写死
        paramKey: "controler_center",
        controlCenter: {
            // 日租订单财务审核配置
            dailyOrderApproval: 1,
            // 自动查询违章 是否开启(1是0否)
            autoQueryIllegalEnable: 0,
            // 自动查询违章每隔x天查询
            autoQueryIllegalDay: 1,
            // 自动查询违章，查询车辆
            autoQueryVehicleConfig: [],
            // 车辆违章预警 是否开启(1是0否)
            illegalConditionEnable: 1,
            // 车辆违章预警条件：1满足全部，2满足其一
            illegalConditionConfig: 1,
            // 每个条件值
            /*
            {
                key 下拉框配套值
                value": 预警值
                unit": 下拉框配套单位
                enable 是否开启该条件
            }
             */
            illegalConditionList: [{
                    "key": 1,
                    "value":0,
                    "unit": "次",
                    "enable": 0
                },
                {
                    "key": 1,
                    "value":0,
                    "unit": "次",
                    "enable": 0
                },
                {
                    "key": 1,
                    "value":0,
                    "unit": "次",
                    "enable": 0
                }]
        },
        illegalConditionConfigDict:[
            {
                "key": 1,
                "label": "满足所有条件"
            },{
                "key": 2,
                "label": "满足任一条件"
            },
        ],
        autoQueryVehicleConfigDict:[
            {
                "key": 0,
                "label": "全部"
            },
            {
                "key": 1,
                "label": "退车"
            },{
                "key": 2,
                "label": "换车"
            },{
                "key": 3,
                "label": "用车"
            },
        ],
        // 未处理违章项条件
        illegalItemDict: [
            {
                "key": 1,
                "label": "车辆未处理违章次数达",
                "unit": "次"
            },
            {
                "key": 2,
                "label": "车辆未处理违章罚款达",
                "unit": "元"
            },
            {
                "key": 3,
                "label": "车辆未处理违章分数达",
                "unit": "分"
            }
        ],
        // 未处理违章项条件，将illegalItemDict属性处理为map结构
        illegalItemMap:{}
    },
    created: function () {
        // 构建map数据方便变更单位
        for(var index in this.illegalItemDict){
            var illegalItem = this.illegalItemDict[index];
            this.illegalItemMap[illegalItem.key] = illegalItem;
        }
        var vueApp = this;
        // 回显数据
        $.get(baseURL + "/sys/tConfig/"+this.paramKey, function (r) {
            if(!r || !r.data){
                return false;
            }
            var controlCenter = r.data;
            vueApp.id = controlCenter.id;
            vueApp.controlCenter = JSON.parse(controlCenter.paramValue);
            // 复选框回显
            var autoQueryVehicleConfig = vueApp.controlCenter.autoQueryVehicleConfig;
            $("[name=autoQueryVehicleConfig]").each((index, value)=>{
                if (autoQueryVehicleConfig.indexOf(value.value) != -1) {
                    $(value).next().click();
                }
            });
        });
    },
    updated: function () {
        layui.form.render();
    },
    methods: {
        saveOrUpdate: function (event) {
            var url = vm.id == null ? "sys/tConfig/save" : "sys/tConfig/update";
            // 发送的数据
            var body = {
                "id": vm.id,
                "paramKey": vm.paramKey,
                "paramValue": JSON.stringify(vm.controlCenter),
                "remark": "商户配置中心所有"
            }
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(body),
                success: function(r){
                    if(r.code === 0){
                        alert('操作成功');
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
    }
});