$(function () {
    layui.config({
        base: '../../statics/common/'
    }).extend({
        soulTable: 'layui/soultable/ext/soulTable.slim'
    });
    layui.use(['form', 'layedit', 'laydate', 'element', 'table', 'soulTable'], function(){
        init(layui);
        layui.form.render();
    });
});

var vm = new Vue({
    el:'#rrapp',
    data:{
        tenantEntity: {}
    },
    created: function(){
        var _this = this;
        var param = parent.layer.boxParams.boxParams;
        _this.tenantEntity = param.data;
    },
    computed:{
        getTenantFlag: {
            get: function () {
                if (this.tenantEntity.tenantFlag == 0) {
                    return "正常";
                } else if (this.tenantEntity.tenantFlag == 1) {
                    return "停用";
                } else {
                    return "--";
                }
            }
        },
        getBigScreenFlag: {
            get: function () {
                if (this.tenantEntity.bigScreenFlag == 0) {
                    return "开启";
                } else if (this.tenantEntity.bigScreenFlag == 1) {
                    return "关闭";
                } else {
                    return "--";
                }
            }
        },
        getTenantType: {
            get: function () {
                if (this.tenantEntity.tenantType == 0) {
                    return "平台";
                } else if (this.tenantEntity.tenantType == 1) {
                    return "商户";
                } else {
                    return "--";
                }
            }

        }

    },
    updated: function(){
        layui.form.render();
    },
    methods: {

    }
});

function init(layui) {
    initEventListener(layui);
}

function initEventListener(layui) {
    initClick();
}

function initClick() {
    $("#closePage").on('click', function(){
        closePage();
    });
}

function closePage() {
    parent.vm.isClose = true;
    var index = parent.layer.getFrameIndex(window.name);
    parent.vm.reload();
    parent.layer.close(index);
}
