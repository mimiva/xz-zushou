$(function () {
    layui.config({
        base: '../../statics/common/'
    }).extend({
        soulTable: 'layui/soultable/ext/soulTable.slim'
    });
    layui.use(['form', 'layedit', 'laydate', 'element', 'table', 'soulTable'], function(){
        init(layui);
        layui.form.render();
    });
});

var vm = new Vue({
    el:'#rrapp',
    data:{
        q:{
            keyword: null
        },
        contractemplate: {},
        verify: false
    },
    created: function(){
        var _this = this;
        var param = parent.layer.boxParams.boxParams;
        _this.contractemplate = param.data;
        if (_this.contractemplate == null) {
            _this.contractemplate = {};
        }
        if (_this.contractemplate['operationId'] == null){
            _this.contractemplate['operationId'] = sessionStorage.getItem("userId");
            _this.contractemplate['operationName'] = sessionStorage.getItem("username");
        }
        if (_this.contractemplate['rentType'] == null){
            _this.contractemplate['rentType'] = '';
        }
    },
    updated: function(){
        layui.form.render();
    },
    methods: {
        saveOrUpdate: function (event) {
            var url = vm.contractemplate.id == null ? "contract/contractemplate/save" : "contract/contractemplate/update";
            PageLoading();
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.contractemplate),
                success: function(r){
                    RemoveLoading();
                    if(r.code === 0){
                        alert('操作成功', function(index){
                            closePage();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
    }
});

function init(layui) {
    initTable(layui.table, layui.soulTable);
    initDate(layui.laydate);
    initEventListener(layui);
    initData();
    initUpload(layui.upload);
}

function initUpload(upload) {
    initializeUploadByConf({
        elid: 'addDoc',
        operationId: sessionStorage.getItem("userId"),
        operationName: sessionStorage.getItem("username"),
        fidedesc: '合同模板附件',
        acceptMime: '.doc,.docx',
        exts: 'doc|docx',
        callBack: function (tag, res, fileName) {
            if (tag == 'success') {
                vm.contractemplate.url = res.data[0];
                Vue.set(vm.contractemplate, 'nameFile', fileName);
            }
        }
    });
}

function initData() {

}

function initEventListener(layui) {
    initClick();
    initChecked(layui.form);
    initVerify(layui.form);
}

function initVerify(form) {
    form.verify({
        validate_nameTpl: function (value, item) { //value：表单的值、item：表单的DOM对象
            if (vm.verify) {
                if (value == null || value == '') {
                    vm.verify = false;
                    return "请输入模版名称";
                }
            }
        },
        validate_rentType: function (value, item) { //value：表单的值、item：表单的DOM对象
            if (vm.verify) {
                if (value == null || value == '') {
                    vm.verify = false;
                    return "请选择模版租赁类型";
                }
            }
        }
    });
}

function initChecked(form) {
    form.on('submit(save)', function(){
        vm.saveOrUpdate();
        return false;
    });

    form.on('select(rentType)', function (data) {
        vm.contractemplate.rentType = data.value;
    });
}

function initClick() {
    $("#closePage").on('click', function(){
        closePage();
    });

    $("#save").on('click', function () {
        vm.verify = true;
    });
}

function initTable(table, soulTable) {

    initTableEvent(table);
    initTableEditListner(table);
}

function initTableEditListner(table) {

}

function initTableEvent(table) {

}

function initDate(laydate) {

}

function closePage() {
    parent.vm.isClose = true;
    var index = parent.layer.getFrameIndex(window.name);
    parent.vm.reload();
    parent.layer.close(index);
}
