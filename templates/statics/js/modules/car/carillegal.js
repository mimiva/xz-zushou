// 供应商多选事件
var xmSelectOnFunction = function (data){
    //arr:  当前多选已选中的数据
    var purchaseSuppliers = data.arr;
    vm.commitWorkOrder.deptIdList = [];
    vm.commitWorkOrder.deptNameList = [];
    for (var i in purchaseSuppliers){
        vm.commitWorkOrder.deptIdList.push(purchaseSuppliers[i].purchaseSupplierId);
        vm.commitWorkOrder.deptNameList.push(purchaseSuppliers[i].supplierName);
    }
    vm.commitWorkOrder.deptIdListStr = vm.commitWorkOrder.deptIdList.join(',');
    vm.commitWorkOrder.deptNameListStr = vm.commitWorkOrder.deptNameList.join(',');
}
$(function () {
    layui.config({
        base: '../../statics/common/'
    }).extend({
        soulTable: 'layui/soultable/ext/soulTable.slim'
    });
    layui.use(['form', 'element', 'table', 'soulTable', 'layedit', 'laydate'], function () {
            soulTable = layui.soulTable;
            gridTable = layui.table.render({
            id: "gridid1",
            elem: '#grid',
            url: baseURL + 'car/carsurviel/queryList',
            where: JSON.parse(JSON.stringify(vm.q)),
            cols: [[
                {
                    event:"multipleSelection",
                    type:"checkbox",
                    width: 60,
                    align:"center"
                },
                {field: 'hphm', minWidth: 100, title: '车牌号', align: "center",templet: function (d) {return isEmpty(d.hphm)}},
                {field: 'hpzlStr', minWidth: 100, title: '号牌种类', align: "center",templet: function (d) {return isEmpty(d.hpzlStr)}},
                {field: 'orderCarNo', minWidth: 100, title: '车辆订单号', align: "center",templet: function (d) {return isEmpty(d.orderCarNo)}},
                {field: 'customerName', minWidth: 100, title: '客户名称', align: "center",templet: function (d) {return isEmpty(d.customerName)}},
                {field: 'wfsj', minWidth: 100, title: '违章时间', align: "center",templet: function (d) {return isEmpty(d.wfsj)}},
                {field: 'wfdz', minWidth: 100, title: '违章地点', align: "center",templet: function (d) {return isEmpty(d.wfdz)}},
                {field: 'wfms', minWidth: 100, title: '违章行为', align: "center",templet: function (d) {return isEmpty(d.wfms)}},
                {field: 'clbj', minWidth: 100, title: '是否处理', align: "center",templet: function (d) {return transformTypeByMap(d.clbj, {1:'已处理', 0:'未处理'})}},
                {field: 'clsj', minWidth: 100, title: '处理时间', align: "center",templet: function (d) {return isEmpty(d.clsj)}},
                {field: 'jkbj', minWidth: 100, title: '是否交款', align: "center",templet: function (d) {return transformTypeByMap(d.jkbj, {1:'已交款', 0:'未交款'})}},
                {field: 'cjjgmc', minWidth: 100, title: '采集单位', align: "center",templet: function (d) {return isEmpty(d.cjjgmc)}},
                {field: 'fkje', minWidth: 100, title: '罚款金额', align: "center",templet: function (d) {return isEmpty(d.fkje)}},
                {field: 'wfjfs', minWidth: 100, title: '记分值', align: "center",templet: function (d) {return isEmpty(d.wfjfs)}},
                {field: 'orderCarBusinessType', minWidth: 100, title: '业务状态', align: "center",templet: function (d) {
                        let orderCarBusinessType = isEmpty(d.orderCarBusinessType);
                        if (orderCarBusinessType == '--') {
                            return orderCarBusinessType;
                        }
                        return vm.orderCarBusinessTypeMap[orderCarBusinessType];
                    }
                },
            ]],
            page: true,
            loading: false,
            limits: [10, 20, 50, 100],
            limit: 10,
            autoColumnWidth: {
                init: true
            },
            done: function () {
                soulTable.render(this);
            }
        });
        var columnNameMap = {
            "fkje":"违章罚款",
            "wfjfs+0":"违章扣分",// 此处故意+0，进行数字转化
            "wfsj":"违章时间",
        };
        Search({
            elid: 'searchId',
            vm_q: vm.q,
            termList: [
                {type: 'text', label: '车牌号', placeholder: '请输入车牌号', fieldName: 'carPlateNo',},
                {type: 'text', label: '违章行为', placeholder: '请输入违章内容', fieldName: 'illegalDetail',},
                {type: 'select', label: '处理状态', placeholder: '请选择处理状态', fieldName: 'illegalStatus', selectMap: {
                        0:'未处理',
                        1:'已处理',
                    },
                },
                {type: 'date', label: '违章时间', placeholder: '选择日期范围', fieldName: 'illegalTime', selectFilter: true},
                {type: 'select', label: '筛选字段', placeholder: '请选择筛选字段', fieldName: 'columnName', selectMap: columnNameMap, selectFilter: true},
                {type: 'text', label: '筛选值', placeholder: '请输入条件值，时间格式用yyyy-MM-dd HHH:mm:ss', fieldName: 'columnValue', selectFilter: true},
                {type: 'select', label: '排序字段', placeholder: '请选择排序字段', fieldName: 'sortColumn', selectMap: columnNameMap, selectFilter: true},
                {type: 'select', label: '排序方式', placeholder: '请选择排序方式', fieldName: 'orderType', selectMap: {
                        "ASC":'升序',
                        "DESC":'降序',
                    }, selectFilter: true
                },
            ],
            callback: function (tag) {
                switch (tag) {
                    case 'updateStatistics':{
                        vm.query();
                        break;
                    }
                    case 'query':{
                        vm.query();
                        break;
                    }
                    case 'reset':{
                        vm.reset();
                        break;
                    }
                    case 'exports':{
                        vm.exports();
                        break;
                    }
                }
            }
        }).initView();

        $("#manualQuery").on('click', function () {
            vm.manualQuery();
        });
        layui.form.render();
    });
    vm.updateStatistics();
    //列表复选框多选
    layui.table.on('checkbox(grid)', function () {
        vm.ids = layui.table.checkStatus('gridid1').data.map(item=>item.id);
    });
    layui.form.on('radio(platform)', function(data){
        vm.commitWorkOrder.platform = data.value;
        // 当选择深圳交警时，只有查全部
        if(data.value=='2'){
            vm.commitWorkOrder.queryMode = 0;
        }
    });
    layui.form.on('radio(queryMode)', function(data){
        // 查全部则清空选择部门
        vm.commitWorkOrder.queryMode = data.value;
        if(data.value!='0'){
            return false;
        }
        vm.commitWorkOrder.deptIdList=[];
        vm.commitWorkOrder.deptNameList=[];
        vm.commitWorkOrder.deptNameListStr = '';
        vm.commitWorkOrder.deptIdListStr = '';
        // 清空所有选项
        for (let i = 0; i < vm.purchasesupplierList.length; i++) {
            vm.purchasesupplierList[i]['selected'] = false;
        }
        xmSelect.render({
            el: '#purchaseSupplierId2',
            language: 'zn',
            prop: {
                name: 'supplierName',
                value: 'purchaseSupplierId',
            },
            data: vm.purchasesupplierList,
            // 供应商多选事件
            on: xmSelectOnFunction
        });
    });
});
function isBlank(str){
    // 判断对象是否为空
    return str==null || str==undefined || str=='null' || str=='undefined' || str=='';
}
var setting = {
    data: {
        simpleData: {
            enable: true,
            idKey: "deptId",
            pIdKey: "parentId",
            rootPId: -1
        },
        key: {
            url:"nourl"
        }
    }
};
var ztree;
var vm = new Vue({
    el: '#rrapp',
    data: {
        q: {
            carPlateNo:null,
            illegalDetail:null,
            illegalTime:null,
            violationDay:null,
            orderCarBusinessType:null
        },
        carIllegalCount:[],
        carIllegalIndex: 0,
        editForm: false,
        mainForm: true,
        city: [],   //运营城市城市名称
        carIllegal: {},
        carIllegalHandle: {},
        queryData: {},
        warehouseData:{},
        isFilter:false,
        statisticsCardDataList:[],
        orderCarBusinessTypeStatistics: {},
        orderCarBusinessTypeMap: {
            "1": "退车",
            "2": "换车",
            "3": "用车"
        },
        statisticsCardIndex:0,
        // 复选框勾选
        ids:[],
        //region 违章工单提交
        // 提交工单
        commitWorkOrder:{
            platform: 1,
            queryMode: 0,
            deptIdList: [],
            deptIdListStr: null,
            deptNameList: [],
            deptNameListStr: null,
            carNumber: 0
        },
        // 提交工单的表单
        showForm:false,
        showForm2:false,
        platformDict:[
            {key:1,label:"12123平台"},
            {key:2,label:"深圳交警"}
        ],
        queryModeDictMap:{
            "0":"全部车辆",
            "1":"部分车辆",
        },
        purchasesupplierList:[],
        //endregion
        // 编辑消息模板
        messageTemplateObj:{
            msgType:1,
            templateCode:3,
            templateStatus:1,
            msgContent:"您有条车辆违章预警短信通知：车辆“#{hphm}”目前存在违章数据。违章行为：”#{wfms}”、地点:“#{wfdz}”、 时间“#{wfsj}”、已超过#{daysOverdue}天未处理、扣分“#{wfjfs}”、罚款:“#{fkje}元”，请尽快处理。"
        }
    },
    updated: function () {
        layui.form.render();
    },

    created: function () {

    },

    methods: {
        illegalCheck:function(){
            $.ajax({
                type: "POST",
                url: baseURL + 'car/carsurviel/requestSurviel',
                contentType: "application/json",
                data: JSON.stringify(vm.sysDept),
                success: function(r){
                    if(r && r.msg){
                        alert(r.msg, function(index){
                            layer.closeAll();
                        });
                    }
                }
            });
        },
        isVehicleNumber: function(vehicleNumber){
            var xreg=/^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}(([0-9]{5}[DF]$)|([DF][A-HJ-NP-Z0-9][0-9]{4}$))/;
            var creg=/^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳]{1}$/;
            if(vehicleNumber.length == 7){
                return creg.test(vehicleNumber);
            } else if(vehicleNumber.length == 8){
                return xreg.test(vehicleNumber);
            } else{
                return false;
            }
        },
        // 更新未处理数量统计
        updateStatistics:function (){
            var param = '?';
            for (var key in vm.q) {
                if (isBlank(vm.q[key])) {
                    continue
                }
                param+=`${key}=${vm.q[key]}&`
            }
            $.get(baseURL + "/car/carsurviel/tab"+param, function (r) {
                /*
                r 示例
                {
                    "msg": "success",
                    "code": 0,
                    "data": [
                        {
                            "text": "全部违章",
                            "number": 8
                        },
                        {
                            "text": "超15天未处理",
                            "number": 1,
                            "violationDay": 15
                        },
                        {
                            "text": "超30天未处理",
                            "number": 1,
                            "violationDay": 30
                        }
                    ],
                    "orderCarBusinessTypeStatistics": {
                        "total": 8,
                        "change_car": 1,
                        "lease_renewal": 0,
                        "return_car": 0
                    }
                }
                 */
                if(!r || !r.data){
                    console.log("车辆违章数量统计报错:"+r.msg)
                    return false;
                }
                vm.statisticsCardDataList = r.data;
                vm.statisticsCardDataList.push({orderCarBusinessType:1,text:"退车",number:r.orderCarBusinessTypeStatistics['return_car']||0});
                vm.statisticsCardDataList.push({orderCarBusinessType:2,text:"换车",number:r.orderCarBusinessTypeStatistics['change_car']||0});
                vm.statisticsCardDataList.push({orderCarBusinessType:3,text:"用车",number:r.orderCarBusinessTypeStatistics['lease_renewal']||0});
                vm.orderCarBusinessTypeStatistics = r.orderCarBusinessTypeStatistics;
            });
        },
        changeTab: function (item, index) {
            // 改变tab
            vm.statisticsCardIndex = index;
            vm.q.violationDay = item.violationDay;
            vm.q.orderCarBusinessType = item.orderCarBusinessType;
            // 触发查询
            vm.query();
        },
        manualQuery: function () {//查询
            var param = {};
            var index = layer.open({
                title: "违章查询",
                type: 2,
                area: ['60%', '60%'],
                boxParams: param,
                content: tabBaseURL + "modules/car/selectorcar.html",
                end: function () {
                    layer.close(index);
                }
            });
        },
        // 违章工单列表
        workOrderPage: function () {
            var index = layer.open({
                title: "违章工单",
                type: 2,
                area: ['100%', '100%'],
                content: tabBaseURL + "modules/car/illegal/workOrderList.html",
                end: function () {
                    layer.close(index);
                }
            });
        },
        // 违章查询账号列表
        illegalQueryAccountPage: function () {
            var index = layer.open({
                title: "违章账号",
                type: 2,
                area: ['100%', '100%'],
                content: tabBaseURL + "modules/car/illegal/workOrderAccountList.html",
                end: function () {
                    layer.close(index);
                }
            });
        },
        reset: function () {//清空方法
            // 不重置tab值
            resetNULL(vm.q);
            vm.q.violationDay = vm.statisticsCardDataList[vm.statisticsCardIndex].violationDay;
        },
        query: function () {
            vm.reload();
        },
        exports: function () {
            window.location.href = urlParamByObj(baseURL + 'car/carsurviel/export', vm.q);
        },
        reload: function (event) {
            layui.table.reload('gridid1', {
                page: {
                    curr: 1
                },
                where: JSON.parse(JSON.stringify(vm.q))
            });
            // 更新统计行
            vm.updateStatistics();
        },
        // 手动短信通知
        manualSmsNotification:function (){
            vm.q.ids = vm.ids;
            confirm('确认手动短信通知？', function () {
                $.ajax({
                    type: "POST",
                    url: baseURL + "/car/carsurviel/manualSmsNotification",
                    contentType: "application/json",
                    data: JSON.stringify(vm.q),
                    success: function (r) {
                        if (r.code === 0) {
                            layer.closeAll();
                            vm.query();
                            return false;
                        }
                        alert(r.msg);
                    }
                });
            });
        },
        // 消息通知
        notice:function(){
            var index = layer.open({
                title: "车辆违章通知",
                type: 2,
                area: ['80%', '80%'],
                content: tabBaseURL+'modules/maintenance/messageNotice.html',
                end: function(){
                    layer.close(index);
                    window.localStorage.setItem("carId",null);
                }
            });
        },
        // 弹出编辑消息模板页面
        editMessageTemplatePage:function (){
            var url = `${baseURL}/message/messagetemplate/queryList?page=1&limit=1&msgType=1&templateCode=3`;
            $.get(url, function (r) {
                var list = r.data;
                if(list==null || list.length<1){
                    return false;
                }
                vm.messageTemplateObj = list[0];
            });
            var index = layer.open({
                title: "编辑消息模板",
                type: 1,
                area: ['70%', '40%'],
                content: $("#editForm2"),
                end: function () {
                    vm.showForm2 = false;
                    layer.closeAll();
                }
            });
            vm.showForm2 = true;
        },
        // 编辑消息模板
        editMessageTemplate:function (){
            $.ajax({
                type: "POST",
                url: `${baseURL}/message/messagetemplate/${vm.messageTemplateObj.id==null?"save":"update"}`,
                contentType: "application/json",
                data: JSON.stringify(vm.messageTemplateObj),
                success: function (r) {
                    if (r.code === 0) {
                        layer.closeAll();
                        vm.query();
                        return false;
                    }
                    alert(r.msg);
                }
            });
        },
        //region 提交工单
        // 更新供应商列表
        selectSupList: function (){
            // 供应商列表
            $.get(baseURL + "purchase/purchasesupplier/selectSupList?enalbe=1", function (r) {
                vm.purchasesupplierList = r.data;
                // 回显数据
                let deptIdList = vm.commitWorkOrder.deptIdList;
                if(deptIdList && deptIdList.length>0 && vm.purchasesupplierList.length>0){
                    for (let i = 0; i < vm.purchasesupplierList.length; i++) {
                        vm.purchasesupplierList[i]['selected'] = deptIdList.indexOf(vm.purchasesupplierList[i].purchaseSupplierId)!=-1
                    }
                }
                vm.selectSupListObj = xmSelect.render({
                    el: '#purchaseSupplierId2',
                    language: 'zn',
                    prop: {
                        name: 'supplierName',
                        value: 'purchaseSupplierId',
                    },
                    data: r.data,
                    // 供应商多选事件
                    on: xmSelectOnFunction
                });
            });
        },
        // 查询违章平台配置
        queryPlatformConfig:()=>{
            console.log(`查询违章平台配置`)
            $.ajaxSettings.async = false;
            $.get(baseURL + "/car/workOrderAccount/accountEnableFlagStatistics", function (r) {
                if(!r || !r.data){
                    return false;
                }
                var result = r.data;
                vm.enableAccountNumber = r.data;
                vm.platformConfig = 0;
                // 仅12123
                if(result['1']>0 && result['2']==0){
                    vm.platformConfig = 1;
                }else if(result['1']==0 && result['2']>0){
                    // 仅深圳交警
                    vm.platformConfig = 2;
                    vm.commitWorkOrder.platform=2;
                }else if(result['1']>0 && result['2']>0){
                    // 12123+深圳交警
                    vm.platformConfig = 3;
                }
            });
            $.ajaxSettings.async = true;
        },
        // 提交工单
        submit: function (){
            // 统计启用账号违章情况
            vm.queryPlatformConfig();
            // 启用账号里只有深圳交警
            if(vm.platformConfig == 0){
                alert('无启用账号，禁止提交工单');
                return false;
            }
            // 启用账号里只有深圳交警
            if(vm.platformConfig == 2){
                vm.submit1();
                return false;
            }
            // 供应商初始化
            vm.selectSupList();
            var index = layer.open({
                title: "提交工单",
                type: 1,
                area: ['40%', '40%'],
                content: $("#editForm"),
                end: function () {
                    vm.showForm = false;
                    layer.closeAll();
                }
            });
            vm.showForm = true;
            // 初始化提交信息
            vm.commitWorkOrder = {
                platform: 1,
                queryMode: 0,
                deptIdList: [],
                deptIdListStr: null,
                deptNameList: [],
                deptNameListStr: null,
                carNumber: 0
            };
        },
        // 启用账号里只有深圳交警
        submit1:function (){
            // 交管账号查询车辆总数
            var number = vm.enableAccountNumber['2']*10;
            if (vm.commitWorkOrder.carNumber > number) {
                alert("查询车辆超过交管账号查询上限");
                return false;
            }
            // 最终发送前检查处理一遍数据
            vm.checkData();
            confirm('确认提交违章查询工单？', function () {
                $.ajax({
                    type: "POST",
                    url: baseURL + "/car/illegalWorkOrder/save",
                    contentType: "application/json",
                    data: JSON.stringify(vm.commitWorkOrder),
                    success: function (r) {
                        if (r.code === 0) {
                            layer.closeAll();
                            vm.query();
                            return false;
                        }
                        alert(r.msg);
                    }
                });
            });
        },
        // 提交工单
        commitWorkOrderEvent: function () {
            // 交管账号查询车辆总数，一个账号可以查10个车
            var number = vm.enableAccountNumber['2']*10;
            if (vm.commitWorkOrder.platform==2 && vm.commitWorkOrder.carNumber > number) {
                alert("查询车辆超过交管账号查询上限");
                return false;
            }
            // 最终发送前检查处理一遍数据
            vm.checkData();
            $.ajax({
                type: "POST",
                url: baseURL + "/car/illegalWorkOrder/save",
                contentType: "application/json",
                data: JSON.stringify(vm.commitWorkOrder),
                success: function (r) {
                    if (r.code === 0) {
                        layer.closeAll();
                        vm.query();
                        return false;
                    }
                    alert(r.msg);
                }
            });
        },
        // 检查数据，校验数据业务合法
        checkData:function (){
            // 最终发送前检查处理一遍数据
            if(vm.commitWorkOrder.platform==1){
                // 12123
                if(vm.commitWorkOrder.queryMode==0){
                    // 全部车辆，不看所属公司
                    vm.commitWorkOrder.deptIdList=[];
                    vm.commitWorkOrder.deptNameList=[];
                    vm.commitWorkOrder.deptNameListStr = '';
                    vm.commitWorkOrder.deptIdListStr = '';
                }
            }else if(vm.commitWorkOrder.platform==2){
                // 深圳交警，默认全部车辆
                vm.commitWorkOrder.queryMode=0;
            }
        },
        //endregion
    }
});
