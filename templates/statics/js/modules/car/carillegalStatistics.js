$(function () {
    gridTable = layui.table.render({
        id: "gridid1",
        elem: '#grid',
        url: baseURL + '/car/carsurviel/carillegalStatistics',
        where: vm.q,
        cols: [[
            {field: 'carNo', minWidth: 70, title: '车牌号', align: "center", templet: d=>isEmpty(d.carNo)},
            {field: 'carBrandSeriesModelName', minWidth: 150, title: '车型', align: "center", templet: d=>isEmpty(d.carBrandSeriesModelName)},
            {field: 'illegalStatisticsCount', minWidth: 100, title: '违章累积次数', align: "center", templet: d=>isEmpty(d.illegalStatisticsCount)},
            {field: 'illegalStatisticsFine', minWidth: 100, title: '违章累积罚款', align: "center", templet: d=>isEmpty(d.illegalStatisticsFine)},
            {field: 'illegalStatisticsPoints', minWidth: 100, title: '违章累积扣分', align: "center", templet: d=>isEmpty(d.illegalStatisticsPoints)},
            {field: 'untreatedIllegalStatisticsCount', minWidth: 100, title: '未处理违章累积次数', align: "center", templet: d=>isEmpty(d.untreatedIllegalStatisticsCount)},
            {field: 'untreatedIllegalStatisticsFine', minWidth: 100, title: '未处理违章累积罚款', align: "center", templet: d=>isEmpty(d.untreatedIllegalStatisticsFine)},
            {field: 'untreatedIllegalStatisticsPoints', minWidth: 100, title: '未处理违章累积次数', align: "center", templet: d=>isEmpty(d.untreatedIllegalStatisticsPoints)},
            {field: 'lastUpdateTime', minWidth: 100, title: '更新时间', align: "center", templet: d=>isEmpty(d.lastUpdateTime)},
        ]],
        page: true,
        loading: false,
        limits: [10, 20, 50, 100],
        limit: 10,
        autoColumnWidth: {
            init: true
        }
    });
    //是否达到预警值，相当于layui-select的事件
    layui.form.on('select(warningFlag)', function (data) {
        vm.q.warningFlag = data.value;
    });
    // 加载渲染所有layui组件
    layui.form.render();
});
var vm = new Vue({
    el: '#rrapp',
    data: {
        q: {
            carNo:null,
            warningFlag:null
        },
        warningFlagDict:[
            {value:"1",label:"是"},
            {value:"0",label:"否"}
        ]
    },
    updated: function () {
        layui.form.render();
    },
    methods: {
        reset: function () {//清空方法
            resetNULL(vm.q);
        },
        query: function () {
            layui.table.reload('gridid1', {
                page: {
                    curr: 1
                },
                where: vm.q
            });
        }
    }
});
