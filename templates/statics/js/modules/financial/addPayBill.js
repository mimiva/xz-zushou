$(function () {
    layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form;
        layer = layui.layer,
            layedit = layui.layedit,
            laydate = layui.laydate;
            upload = layui.upload;
        form.verify({
            /*paidAmountVerify: function (value) {
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                if(value==''){
                    return '应还总金额不能为空';
                } else {
                    if(!reg.test(value)){
                        return '应还总金额输入格式不正确(大于0或保留两位小数的正整数)!';
                    }
                }
            },
            playStartDateVerify: function (value) {
                if(value==''){
                    return '请选择开始时间';
                }
            },
            playEndDateVerify: function (value) {
                if(value==''){
                    return '请选择结束时间';
                }
            },
            paidDateVerify: function (value) {
                if(value==''){
                    return '请选择应还日期';
                }
            },*/
        });

        laydate.render({
            elem: '#paymentTime',
            trigger: 'click',
            done: function (value, date, endDate) {
                vm.payment.paymentTime=value;
            }
        });
        uploadAttachment(upload);

        form.render();
    });
    layui.form.on('submit(saveOrUpdate)', function(){
        vm.saveOrUpdate();
        return false;
    });
});

var vm = new Vue({
    el:'#rrapp',
    data:{
        status:null,
        title:null,
        amountLabel:null,
        dateLabel: null,
        payment:{},
        carSourceOrder:{},
        deliveryFileLst: [],
        deliveryFileLstId: 'deliveryFileLstId_0',
    },
    updated: function(){
        layui.form.render();
    },
    methods: {
        cancel:function(){
            parent.layer.closeAll();
        },
        delDeliveryFile: function (id) {
            for (var i = 0; i < vm.deliveryFileLst.length; i++) {
                if (vm.deliveryFileLst[i].id === id) {
                    vm.deliveryFileLst.splice(i, 1);
                    i = i - 1;
                }
            }
        },
        //保存
        saveOrUpdate: function (event) {
            vm.payment.attachment = vm.deliveryFileLst;
            PageLoading();
            //confirm('确定要新增'+title+'吗？', function(){
                $.ajax({
                    type: "POST",
                    url: baseURL + "pay/carsourceorder/savePayBill",
                    contentType: "application/json",
                    data: JSON.stringify(vm.payment),
                    success: function(r){
                        RemoveLoading();
                        if(r.code === 0){
                            alert('操作成功', function(index){
                                layer.closeAll();
                                var index = parent.layer.getFrameIndex(window.name);
                                parent.layer.close(index);
                                if(vm.payment.status == 1){
                                    parent.vm.reloadPayBillPage(vm.carSourceOrder.id, 1);
                                } else if(vm.payment.status == 2){
                                    parent.vm.reloadPaidBillPage(vm.carSourceOrder.id,2);
                                }
                            });
                        }else{
                            alert(r.msg);
                        }
                    }
                });
            //});
        }
    }
});

//编辑时父页面传过来的值
function sendData(data,type) {
    vm.carSourceOrder = data;
    if (type == 1) {
        vm.payment.status = 1;
        vm.title = "新增应付账款";
        vm.status = "未付款";
        vm.amountLabel = "应付金额";
        vm.dateLabel = "应付日期";
    } else if (type == 2) {
        vm.payment.status = 2;
        vm.title = "新增已付账款";
        vm.status = "已付款";
        vm.amountLabel = "已付金额";
        vm.dateLabel = "已付日期";
    }
    vm.payment.carNo = data.carNo;
    vm.payment.vinNo = data.vinNo;
    vm.payment.supplierId = data.supplierId;
    vm.payment.supplierName = data.supplierName;
    vm.payment.carId = data.carId;
    vm.payment.sourceOrderId = data.id;
    vm.payment.carSource = data.carSourceType;
    vm.payment.contractNo = data.contractNo;
    vm.payment = Object.assign({}, vm.payment,{
        carId:data.carId,
        carNo:data.carNo,
        vinNo:data.vinNo,
        sourceOrderId:data.id,
        supplierId:data.supplierId,
        supplierName:data.supplierName,
        carSource:data.carSourceType,
        contractNo:data.contractNo
    });
    layui.form.render();
}

/**
 * 上传附件
 */
function uploadAttachment(upload) {
    upload.render({
        elem: '#addDeliveryFile',
        url: baseURL + 'file/uploadFile',
        data: {'path': 'order-delivery'},
        field: 'files',
        auto: true,
        size: 50 * 1024 * 1024,
        accept: 'file', //普通文件
        acceptMime: '.pdf,.doc,.docx,.exl,.xlsx,.jpg,.png,.jpeg',
        exts: 'pdf|doc|docx|exl|xlsx|jpg|png|jpeg', //
        multiple: true,
        number:20,
        choose: function (obj) {
            PageLoading();
            obj.preview(function (index, file, result) {
                var fileName = file.name;
                var extIndex = fileName.lastIndexOf('.');
                var ext = fileName.slice(extIndex);
                var fileNameNotext = fileName.slice(0, extIndex);
                var regExt = /png|jpg|jpeg/;
                var fileType = regExt.test(ext) ? 1 : 0;
                deliveryFileIdTmp = vm.deliveryFileLst.length + '_' + uuid(60);
                var fileTmp = {
                    id: deliveryFileIdTmp,
                    nameDesc: '附件',
                    nameAccessory: fileNameNotext,
                    nameFile: fileName,
                    nameExt: ext,
                    typeFile: fileType,
                };
                vm.deliveryFileLst.push(fileTmp);
            });
        },
        done: function (res) {
            RemoveLoading();
            if (res.code == '0') {
                vm.deliveryFileLst.forEach(function (value) {
                    if (value.id === deliveryFileIdTmp) value.url = res.data[0];
                });
                vm.deliveryFileLstId = 'deliveryFileLstId_' + uuid(6);
            } else {
                layer.msg('上传失败', {icon: 5});
                vm.delDeliveryFile(deliveryFileIdTmp);
            }
            deliveryFileIdTmp = null;
        },
        error: function () {
            RemoveLoading();
            layer.msg('上传失败', {icon: 5});
            vm.delDeliveryFile(deliveryFileIdTmp);
            deliveryFileIdTmp = null;
        }
    });
}

