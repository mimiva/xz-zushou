$(function () {
    layui.use(['form', 'layedit', 'laydate'], function () {
        layui.form.render();
    });
    gridTable = layui.table.render({
        id: "gridid",
        elem: '#grid',
        url: baseURL + 'contract/contractemplate/selectorTemplateList',
        where: JSON.parse(JSON.stringify(vm.q)),
        cols: [[
            {title: '操作', width:200, templet:'#barTpl',fixed:"right",align:"center"},
            {field:'nameTpl', title: '模版名称', minWidth:200, templet: function (d) {return isEmpty(d.nameTpl);}},
            {field:'countUse', title: '使用次数', minWidth:200, templet: function (d) {return isEmpty(d.countUse);}},
            {field:'desc', title: '模版备注', minWidth:200, templet: function (d) {return isEmpty(d.desc);}},
            {field:'operationName', title: '创建人', minWidth:200, templet: function (d) {return isEmpty(d.operationName);}},
            {field:'timeCreate', title: '创建时间', minWidth:200, templet: function (d) {return dateFormat(d.timeCreate);}}
        ]],
        page: true,
        loading: true,
        limits: [10, 15, 25, 40],
        limit: 10,
    });

    //操作
    layui.table.on('tool(grid)', function(obj){
        var layEvent = obj.event,
            data = obj.data;
        if(layEvent === 'selector'){
            vm.selector(data);
        }else if (layEvent === 'view'){
            vm.view(data);
        }
    });
});

var action;
var callback;

var vm = new Vue({
    el:'#rrapp',
    data:{
        q:{
            nameTpl: null,
            rentType: null,
            status: 1
        }
},
created: function(){
    var _this = this;
    var param = parent.layer.boxParams.boxParams;
    _this.q.rentType = param.rentType;
    action = param.action;
    callback = param.callback;
},
updated: function(){
    layui.form.render();
},
methods: {
    query: function () {
        vm.reload();
    },
    reset: function () {
        vm.q.nameTpl = null;
    },
    view: function (_data) {
        var param = {
            data:_data
        };
        var index = layer.open({
            id: 'contractemplateview',
            title: "查看",
            type: 2,
            boxParams: param,
            content: [tabBaseURL + "modules/contract/contractemplateview.html", 'no'],
            end: function () {
                layer.close(index);
            }
        });
        layer.full(index);
    },
    selector: function (data) {
        if (('regenerateDocSelector' == action || 'callback' == action) && null != callback){
            callback(data.id, data.nameTpl);
        } else {
            parent.vm.order.contract = Object.assign({}, parent.vm.order.contract, {
                templateId: data.id,
                templateName: data.nameTpl
            });
            parent.vm.contractModelId = 'contractModelId_' + uuid(6);
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        }
    },
    reload: function (event) {
        layui.table.reload('gridid', {
            page: {
                curr: 1
            },
            where: JSON.parse(JSON.stringify(vm.q))
        });
    }
}
});
