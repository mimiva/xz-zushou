$(function () {
    layui.config({
        base: '../../statics/common/'
    }).extend({
        soulTable: 'layui/soultable/ext/soulTable.slim'
    });
    layui.use(['form', 'layedit', 'laydate', 'element', 'table', 'soulTable'], function(){
        init(layui);
        layui.form.render();
    });
});

var vm = new Vue({
    el:'#rrapp',
    data:{
        oldOrderCar:{},
        newOrderCar:{}
    },
    created: function(){
        var _this = this;
        var param = parent.layer.boxParams.boxParams;
        var id = param.id;
        $.ajaxSettings.async = false;
        $.get(baseURL + "order/order/info/" + id, function (r) {
            _this.oldOrderCar = {
                id: r.order.orderCar.id,
                code:r.order.orderCar.code,
                carNo:r.order.orderCar.carNo,
                timeStartRent:r.order.orderCar.timeStartRent,
                timeFinishRent:r.order.orderCar.timeFinishRent,
                freeDays:r.order.plan.freeDays||0,
                paymentDay:r.order.orderCar.paymentDay,
                deposit:new Number(r.order.plan.downPayment||0) + new Number(r.order.plan.cashDeposit||0) + new Number(r.order.plan.servicingFee||0),
                averageRental:new Number(r.order.plan.monthlyRent||0) - new Number(r.order.plan.coverCharge||0),
                coverCharge:r.order.plan.coverCharge||0,
                paymentMethodStr:r.order.orderCar.paymentMethodStr,
                rentType:r.order.orderCar.rentType,
                rentGenerationMethod:r.order.plan.rentGenerationMethod^0,
            }
            _this.newOrderCar = {
                id: r.order.orderCar.id,
                createReceive:2,
                hasReceive:2,
                rentType:r.order.orderCar.rentType,
                rentGenerationMethod:r.order.plan.rentGenerationMethod^0,
            }
        });
        $.ajaxSettings.async = true;
    },
    updated: function(){
        layui.form.render();
    },
    methods: {
        saveOrUpdate: function () {
            //若未填入任何数据，不进行提交
            if((vm.newOrderCar.timeStartRent == null || vm.newOrderCar.timeStartRent == '') &&
                (vm.newOrderCar.timeFinishRent == null || vm.newOrderCar.timeFinishRent == '') &&
                (vm.newOrderCar.freeDays == null || vm.newOrderCar.freeDays == '') &&
                (vm.newOrderCar.paymentDay == null || vm.newOrderCar.paymentDay == '') &&
                (vm.newOrderCar.deposit == null || vm.newOrderCar.deposit == '') &&
                (vm.newOrderCar.averageRental == null || vm.newOrderCar.averageRental == '') &&
                (vm.newOrderCar.coverCharge == null || vm.newOrderCar.coverCharge == '') &&
                (vm.newOrderCar.paymentMethod == null || vm.newOrderCar.paymentMethod == '')
            ){
                alert('未填写修改订单信息');
                return false;
            }

            //只有付款日修改，可以不影响之前账单数据
            if((vm.newOrderCar.timeStartRent != null && vm.newOrderCar.timeStartRent != '') ||
                (vm.newOrderCar.timeFinishRent != null && vm.newOrderCar.timeFinishRent != '') ||
                (vm.newOrderCar.freeDays != null && vm.newOrderCar.freeDays != '') ||
                (vm.newOrderCar.deposit != null && vm.newOrderCar.deposit != '') ||
                (vm.newOrderCar.averageRental != null && vm.newOrderCar.averageRental != '') ||
                (vm.newOrderCar.coverCharge != null && vm.newOrderCar.coverCharge != '') ||
                (vm.newOrderCar.paymentMethod != null && vm.newOrderCar.paymentMethod != '')){
                if(vm.newOrderCar.createReceive == 2){
                    alert('只有修改付款日不影响历史账单数据,必须选择生成新账单');
                    return false;
                }
            }

            if(vm.newOrderCar.createReceive != null && vm.newOrderCar.createReceive == 1){
                layer.confirm("因修改业务信息，已有的账单会被删除，重新生成新的账单，是否确定？",function(){
                    vm.commonSave();
                });
            }else{
                vm.commonSave();
            }

        },
        commonSave:function(){
            var url = "order/ordercar/newUpdate";
            PageLoading();
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.newOrderCar),
                success: function(r){
                    RemoveLoading();
                    if(r.code === 0){
                        alert('操作成功', function(index){
                            closePage();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        }
    }
});

function init(layui) {
    initTable(layui.table, layui.soulTable);
    initDate(layui.laydate);
    initEventListener(layui);
    initData();
    initUpload(layui.upload);
}

function initUpload(upload) {

}

function initData() {

}

function initEventListener(layui) {
    initClick();
    initChecked(layui.form);
    initVerify(layui.form);
}

function initVerify(form) {
    form.verify({
        moneyVerify:function(value){
            if(value != null && value != ''){
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                if(reg.test(value) == false){
                    return "输入值不是正确金额";
                }
            }
        },
        daysVerify:function(value) {
            if (value != null && value != '') {
                var regNumber = /^\+?[1-9][0-9]*$/;
                if (regNumber.test(value) == false) {
                    return "免费天数必须是大于0的整数";
                }
            }
        },
        validate_firstRent: function (value) {
            if (vm.newOrderCar.rentGenerationMethod == 4) {
                if (value == null || value === '') {
                    return "请输入首期租金";
                }
            }
        },
    });
}

function initChecked(form) {
    form.on('radio(paymentMethod)', function (data) {
        vm.newOrderCar.paymentMethod = data.value;
        vm.newOrderCar.paymentMethodStr = data.elem.title;
    });
    form.on('radio(createReceive)', function (data) {
        vm.newOrderCar.createReceive = data.value;
    });
    form.on('radio(hasReceive)', function (data) {
        vm.newOrderCar.hasReceive = data.value;
    });
    form.on('radio(rentGenerationMethod)', function (data) {
        vm.newOrderCar.rentGenerationMethod = data.value;
    });
    form.on('submit(save)', function(){
        vm.saveOrUpdate();
        return false;
    });
}

function initClick() {
    $("#closePage").on('click', function(){
        closePage();
    });
}

function initTable(table, soulTable) {

    initTableEvent(table);
    initTableEditListner(table);
}

function initTableEditListner(table) {

}

function initTableEvent(table) {

}

function initDate(laydate) {
    var timeStartRent = laydate.render({
        elem: '#timeStartRent',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.newOrderCar.timeStartRent = value;
            var month = date.month -1;
            timeFinishRent.config.min = date;
            timeFinishRent.config.min.month = month;
        }
    });

    var timeFinishRent = laydate.render({
        elem: '#timeFinishRent',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.newOrderCar.timeFinishRent = value;
            var month = date.month -1;
            timeStartRent.config.max = date;
            timeStartRent.config.max.month = month;
        }
    });

    laydate.render({
        elem: '#paymentDay',
        trigger: 'click',
        theme: 'grid',
        type: 'date',
        isInitValue: false,
        showBottom: false,
        value: '1989-10-01',
        min: '1989-10-01',
        max: '1989-10-31',
        done: function (value, date) {
            Vue.set(vm.newOrderCar, "paymentDay", date.date);
            $('input#paymentDayVal').val(date.date);
        },
        ready: function(){//
            $('.laydate-theme-grid>div.layui-laydate-hint').hide();
            $('.laydate-theme-grid>div>div.layui-laydate-header').hide();
            $('.laydate-theme-grid>div>div.layui-laydate-content>table>thead').hide();
            $('.laydate-theme-grid>div>div.layui-laydate-content>table>tbody>tr>td.laydate-disabled').hide();
        }
    });
}

function closePage() {
    parent.vm.isClose = true;
    var index = parent.layer.getFrameIndex(window.name);
    parent.vm.reload();
    parent.layer.close(index);
}
