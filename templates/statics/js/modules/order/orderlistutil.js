var dataUrl = baseURL + 'order/order/queryList';
var searchView;
var tableView;
/** 查询条件所有字段 */
var searchMap = {
    /*额外条件*/           /*默认条件*/             /*高级条件*/                     /*高级条件*/
    statusType: '',      carvinno: null,         contractorderCode: null,        customerType: '',
    orderCode: null,     customerName: null,     lessorId: '',                   returnType: '',
                         rentType: '',           modelId: null,                  reletOrderStatus: '',
                                                                                 channelId:null,
                                                                                 salePersonId:null,
    /*租赁开始时间*/                /*租赁结束时间*/                  /*交车时间*/                  /*订单发起时间*/
    timeStartRent: null,          timeFinishRent: null,          timeDelivery: null,          timeCreate: null,
    timeStartRentstart: null,     timeFinishRentstart: null,     timeDeliverystart: null,     timeCreatestart: null,
    timeStartRentend: null,       timeFinishRentend: null,       timeDeliveryend: null,       timeCreateend: null,
    /*退车时间*/                    /*退车发起时间*/                 /*换车时间*/                  /*换车发起时间*/
    timeReturn: null,             timeApplyReturn: null,         timeSwop: null,              timeApplySwop: null,
    timeReturnstart: null,        timeApplyReturnstart: null,    timeSwopstart: null,         timeApplySwopstart: null,
    timeReturnend: null,          timeApplyReturnend: null,      timeSwopend: null,           timeApplySwopend: null,
    /*预计过户时间*/                       /*实际过户时间*/                     /*撤回时间*/
    timeTransferEstimated: null,         timeTransferActual: null,         timeRecall: null,
    timeTransferEstimatedstart: null,    timeTransferActualstart: null,    timeRecallstart: null,
    timeTransferEstimatedend: null,      timeTransferActualend: null,      timeRecallend: null,
    /*续租开始时间*/                     /*续租结束时间*/                       /*暂存时间*/
    timeStartRentRelet: null,          timeFinishRentRelet: null,           timeSave: null,
    timeStartRentReletstart: null,     timeFinishRentReletstart: null,      timeSavestart: null,
    timeStartRentReletend: null,       timeFinishRentReletend: null,        timeSaveend: null,
    /*排序*/
    contractTenancySort: null,//合同租期
    timeStartRentSort: null,//租赁开始日
    timeFinishRentSort: null,//租赁结束日
    timeDeliverySort: null,//交车时间
    usrDaysSort: null,//已使用天数
    saveDaysSort: null,//已暂存天数
    spareCarUsrDaysSort: null,//备用车已使用天数
    timeReturnSort: null,//退车时间
    timeApplyReturnSort: null,//退车发起时间
    timeSwopSort: null,//换车时间
    timeApplySwopSort: null,//换车发起时间
    timeTransferEstimatedSort: null,//预计过户时间
    overdueTransferDaysSort: null,//过户逾期天数
    timeTransferActualSort: null,//实际过户时间
    timeRecallSort: null,//撤回时间
    timeAuditSort: null,//审批时间
    timeCreateSort: null,//订单发起时间
    timeStartRentReletSort: null,//续租开始时间
    timeFinishRentReletSort: null,//续租结束时间
};

var statusCols = [
    {name: '全部订单', count: '', rank: '10', key: '0'},
    {name: '提单审核中', count: '', rank: '20', key: '100'},
    {name: '退车审核中', count: '', rank: '30', key: '500'},
    {name: '换车审核中', count: '', rank: '40', key: '300'},
    {name: '用车中', count: '', rank: '50', key: '200'},
    {name: '已退车', count: '', rank: '60', key: '600'},
    {name: '暂存', count: '', rank: '70', key: '998'},
    {name: '融租/以租代购-未过户', count: '', rank: '110', key: '710'},
    {name: '融租/以租代购-已过户', count: '', rank: '120', key: '711'},
    {name: '直购-未过户', count: '', rank: '130', key: '720'},
    {name: '直购-已过户', count: '', rank: '140', key: '721'},
    {name: '已换车', count: '', rank: '150', key: '400'},
    {name: '待交车', count: '', rank: '160', key: '301'},
    {name: '备用车使用中', count: '', rank: '170', key: '201'},
    {name: '提单已撤回', count: '', rank: '180', key: '101'},
    {name: '提单已拒绝', count: '', rank: '190', key: '102'},
    {name: '已续租', count: '', rank: '210', key: '900'},
    {name: '续租时间未开始', count: '', rank: '220', key: '901'}
];

/** 查询条件列 */
var searchCols = [
    {type: 'text', label: '车牌号/车架号', placeholder: '请输入车牌号/车架号', fieldName: 'carvinno'},
    {type: 'text', label: '客户名称', placeholder: '请输入客户名称', fieldName: 'customerName'},
    {type: 'select', label: '订单类型', fieldName: 'rentType', selectMap: {
        1:'经租', 2:'以租代购', 3:'展示车', 4:'试驾车', 5:'融租', 6:'直购', 7:'挂靠',
        }
    },
    {type: 'select', label: '下单方式', fieldName: 'orderWay', selectMap: {
        1:'代客下单', 2:'客户自主下单',
        }
    },
    {type: 'text', label: '合同编号/订单编号', placeholder: '请输入合同编号/订单编号', fieldName: 'contractorderCode', selectFilter: true},
    {type: 'select', label: '出租方/售卖方', fieldName: 'lessorId', selectListValueName: 'deptId', selectListTxtName: 'name', selectList: [], selectFilter: true},
    {type: 'selectcascader', label: '品牌/车系/车型', placeholder: '请选择品牌/车系/车型', fieldName: 'modelId', selectList: [], selectFilter: true},
    {type: 'select', label: '销售员', fieldName: 'salePersonId', selectListValueName: 'userId', selectListTxtName: 'username', selectList: [], selectFilter: true},
    {type: 'select', label: '客户类型', fieldName: 'customerType', selectMap: {1:'企业', 2:'个人',}, selectFilter: true},
    {type: 'select', label: '渠道商', fieldName: 'channelId', selectListValueName: 'channelId', selectListTxtName: 'channelName', selectList: [], selectFilter: true},
    {hidden: true, type: 'select', label: '退车类别', fieldName: 'returnType', selectMap: {1:'租赁到期', 2:'违约退车', 3:'强制收车',4:'其他'}, selectFilter: true},
    {hidden: true, type: 'select', label: '续租订单状态', fieldName: 'reletOrderStatus', selectMap: {
            500:'退车审核中', 300:'换车审核中', 200:'用车中', 600:'已退车', 400:'已换车', 700:'已过户', 901:'续租时间未开始'
        }, selectFilter: true
    },

    {hidden: true, type: 'date', label: '租赁开始时间', placeholder: '租赁开始时间范围', fieldName: 'timeStartRent', selectFilter: true},
    {hidden: true, type: 'date', label: '租赁结束时间', placeholder: '租赁结束时间范围', fieldName: 'timeFinishRent', selectFilter: true},
    {hidden: true, type: 'date', label: '交车时间', placeholder: '交车时间范围', fieldName: 'timeDelivery', selectFilter: true},
    {hidden: true, type: 'date', label: '订单发起时间', placeholder: '订单发起时间范围', fieldName: 'timeCreate', selectFilter: true},
    {hidden: true, type: 'date', label: '退车时间', placeholder: '退车时间范围', fieldName: 'timeReturn', selectFilter: true},
    {hidden: true, type: 'date', label: '退车发起时间', placeholder: '退车发起时间范围', fieldName: 'timeApplyReturn', selectFilter: true},
    {hidden: true, type: 'date', label: '换车时间', placeholder: '换车时间范围', fieldName: 'timeSwop', selectFilter: true},
    {hidden: true, type: 'date', label: '换车发起时间', placeholder: '换车发起时间范围', fieldName: 'timeApplySwop', selectFilter: true},
    {hidden: true, type: 'date', label: '暂存时间', placeholder: '暂存时间范围', fieldName: 'timeSave', selectFilter: true},
    {hidden: true, type: 'date', label: '预计过户时间', placeholder: '预计过户时间范围', fieldName: 'timeTransferEstimated', selectFilter: true},
    {hidden: true, type: 'date', label: '实际过户时间', placeholder: '实际过户时间范围', fieldName: 'timeTransferActual', selectFilter: true},
    {hidden: true, type: 'date', label: '撤回时间', placeholder: '撤回时间范围', fieldName: 'timeRecall', selectFilter: true},
    {hidden: true, type: 'date', label: '续租开始时间', placeholder: '续租开始时间范围', fieldName: 'timeStartRentRelet', selectFilter: true},
    {hidden: true, type: 'date', label: '续租结束时间', placeholder: '续租结束时间范围', fieldName: 'timeFinishRentRelet', selectFilter: true},
]

var tableCols = [
    {field: 'btns', title: '操作', width: 120, fixed: 'left', align: 'center', templet: function (d) {return isEmpty(d.buttonList);}},
    {field: 'carNo', title: '车牌号', minWidth: 200, templet: function (d) {return isEmpty(d.carNo);}},
    {field: 'vinNo', title: '车架号', minWidth: 200, templet: function (d) {return isEmpty(d.vinNo);}},
    {field: 'modelNames', title: '品牌/车系/车型', minWidth: 200, templet: function (d) {return isEmpty(d.modelNames);}},
    {field: 'contractCode', title: '合同编号', minWidth: 200, templet: function (d) {return isEmpty(d.contractCode);}},
    {field: 'orderCarCode', title: '车辆订单号', minWidth: 200, templet: function (d) {return isEmpty(d.orderCarCode);}},
    {field: 'orderWay', title: '下单方式', minWidth: 200, templet: function (d) {return transformTypeByMap(d.orderWay, {1:'代客下单', 2:'客户自主下单'});}},
    {field: 'rentType', title: '订单类型', minWidth: 200, templet: function (d) {return getRentTypeStr(d.rentType);}},
    {field: 'statusStr', title: '订单状态', minWidth: 200, templet: function (d) {return isEmpty(d.statusStr);}},
    {field: 'orderCarExplain', title: '订单说明', minWidth: 200, templet: function (d) {return isEmpty(d.orderCarExplain);}},
    {field: 'lessorName', title: '出租方/售卖方', minWidth: 200, templet: function (d) {return isEmpty(d.lessorName);}},
    // {field: 'lessorName', title: '售卖方', minWidth: 200, templet: function (d) {return isEmpty(d.lessorName);}},
    {field: 'customerName', title: '客户名称', minWidth: 200, templet: function (d) {return isEmpty(d.customerName);}},
    {field: 'contractTenancy', title: '合同租期', minWidth: 200, sort: false, templet: function (d) {return isEmpty(d.contractTenancy);}},
    {field: 'timeStartRent', title: '租赁开始日', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeStartRent);}},
    {field: 'timeFinishRent', title: '租赁结束日', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeFinishRent);}},
    {field: 'timeDelivery', title: '交车时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeDelivery);}},
    {field: 'usrDays', title: '已使用天数', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.usrDays);}},
    {field: 'saveDays', title: '已暂存天数', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.saveDays);}},
    {field: 'spareCarUsrDays', title: '备用车已使用天数', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.spareCarUsrDays);}},
    {field: 'returnType', title: '退车类别', minWidth:200, templet: function (d) {return transformTypeByMap(d.returnType, {1:'租赁到期', 2:'违约退车', 3:'强制收车', 4:'其他'});}},
    {field: 'timeReturn', title: '退车时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeReturn);}},
    {field: 'timeApplyReturn', title: '退车发起时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeApplyReturn);}},
    {field: 'swopReason', title: '换车原因', minWidth: 200, templet: function (d) {return isEmpty(d.swopReason);}},
    {field: 'timeSwop', title: '换车时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeSwop);}},
    {field: 'timeApplySwop', title: '换车发起时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeApplySwop);}},
    {field: 'timeTransferEstimated', title: '预计过户时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeTransferEstimated);}},
    {field: 'overdueTransferDays', title: '过户逾期天数', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.overdueTransferDays);}},
    {field: 'timeTransferActual', title: '实际过户时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeTransferActual);}},
    {field: 'recallReason', title: '撤回原因', minWidth: 200, templet: function (d) {return isEmpty(d.recallReason);}},
    {field: 'timeRecall', title: '撤回时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeRecall);}},
    {field: 'refuseReason', title: '拒绝原因', minWidth: 200, templet: function (d) {return isEmpty(d.refuseReason);}},
    {field: 'timeAudit', title: '审批时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeAudit);}},
    {field: 'timeCreate', title: '订单发起时间', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeCreate);}},
    {field: 'orderCarCodeRelet', title: '续租订单号', minWidth: 200, templet: function (d) {return isEmpty(d.orderCarCodeRelet);}},
    {field: 'statusRelet', title: '续租订单状态', minWidth: 200, templet: function (d) {return isEmpty(d.statusRelet);}},
    {field: 'timeStartRentRelet', title: '续租开始日', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeStartRentRelet);}},
    {field: 'timeFinishRentRelet', title: '续租结束日', minWidth: 200, sort: true, templet: function (d) {return isEmpty(d.timeFinishRentRelet);}},
];

function onStatusChangedDataUrl(statusKey) {
    dataUrl = baseURL + 'order/order/queryPagenew/' + statusKey;
}

function onStatusChangedSearchView(statusKey) {
    if (searchView == null) return;
    // searchView.reset();
    switch (parseInt(statusKey)) {
        case 0:{//全部订单
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeDelivery');
            break;
        }
        case 100:{//提单审核中
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeDelivery', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeCreate');
            break;
        }
        case 500:{//退车审核中
            searchView.hideItems('reletOrderStatus', 'timeCreate', 'timeSave', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'returnType', 'timeStartRent', 'timeFinishRent', 'timeDelivery', 'timeReturn', 'timeApplyReturn');
            break;
        }
        case 300:{//换车审核中
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeDelivery', 'timeSwop', 'timeApplySwop');
            break;
        }
        case 200:{//用车中
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeDelivery');
            break;
        }
        case 600:{//已退车
            searchView.hideItems('reletOrderStatus', 'timeCreate', 'timeSave', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'returnType', 'timeStartRent', 'timeFinishRent', 'timeDelivery', 'timeReturn');
            break;
        }
        case 998:{//暂存
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeDelivery', 'timeCreate', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeSave');
            break;
        }
        case 710:{//融租未过户
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeDelivery', 'timeTransferEstimated');
            break;
        }
        case 711:{//融租已过户
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeDelivery', 'timeTransferEstimated', 'timeTransferActual');
            break;
        }
        case 720:{//直购未过户
            searchView.hideItems('rentType', 'returnType', 'reletOrderStatus', 'timeStartRent', 'timeFinishRent', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('timeDelivery', 'timeTransferEstimated');
            break;
        }
        case 721:{//直购已过户
            searchView.hideItems('rentType', 'returnType', 'reletOrderStatus', 'timeStartRent', 'timeFinishRent', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('timeDelivery', 'timeTransferEstimated', 'timeTransferActual');
            break;
        }
        case 400:{//已换车
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeDelivery', 'timeSwop', 'timeApplySwop');
            break;
        }
        case 301:{//待交车
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeDelivery', 'timeSwop', 'timeApplySwop');
            break;
        }
        case 201:{//备用车使用中
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeDelivery');
            break;
        }
        case 101:{//提单已撤回
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeDelivery', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeCreate', 'timeRecall');
            break;
        }
        case 102:{//提单已拒绝
            searchView.hideItems('returnType', 'reletOrderStatus', 'timeDelivery', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall', 'timeStartRentRelet', 'timeFinishRentRelet');
            searchView.showItems('rentType', 'timeStartRent', 'timeFinishRent', 'timeCreate');
            break;
        }
        case 900:{//已续租
            searchView.hideItems('returnType', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall');
            searchView.showItems('rentType', 'reletOrderStatus', 'timeStartRent', 'timeFinishRent', 'timeDelivery', 'timeStartRentRelet', 'timeFinishRentRelet');
            break;
        }
        case 901:{//续租时间未开始
            searchView.hideItems('returnType', 'timeCreate', 'timeSave', 'timeReturn', 'timeApplyReturn', 'timeSwop', 'timeApplySwop', 'timeTransferEstimated', 'timeTransferActual', 'timeRecall');
            searchView.showItems('rentType', 'reletOrderStatus', 'timeStartRent', 'timeFinishRent', 'timeDelivery', 'timeStartRentRelet', 'timeFinishRentRelet');
            break;
        }
    }
}

function onStatusChangedTableView(statusKey) {
    var ragex;
    switch (parseInt(statusKey)) {
        case 0:{//全部订单
            ragex = /(orderCarExplain|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 100:{//提单审核中
            ragex = /(orderCarExplain|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 500:{//退车审核中
            ragex = /(orderCarExplain|usrDays|saveDays|spareCarUsrDays|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 300:{//换车审核中
            ragex = /(orderCarExplain|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 200:{//用车中
            ragex = /(saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 600:{//已退车
            ragex = /(orderCarExplain|saveDays|spareCarUsrDays|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 998:{//暂存
            ragex = /(orderCarExplain|usrDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 710:{//融租未过户
            ragex = /(orderCarExplain|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 711:{//融租已过户
            ragex = /(orderCarExplain|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 720:{//直购未过户
            ragex = /(orderCarExplain|contractTenancy|timeStartRent|timeFinishRent|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 721:{//直购已过户
            ragex = /(orderCarExplain|contractTenancy|timeStartRent|timeFinishRent|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 400:{//已换车
            ragex = /(usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 301:{//待交车
            ragex = /(orderCarExplain|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 201:{//备用车使用中
            ragex = /(usrDays|saveDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 101:{//提单已撤回
            ragex = /(orderCarExplain|contractTenancy|timeStartRent|timeFinishRent|timeDelivery|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|refuseReason|timeAudit|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 102:{//提单已拒绝
            ragex = /(orderCarExplain|contractTenancy|timeStartRent|timeFinishRent|timeDelivery|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|orderCarCodeRelet|statusRelet|timeStartRentRelet|timeFinishRentRelet)/;
            break;
        }
        case 900:{//已续租
            ragex = /(orderCarExplain|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate)/;
            break;
        }
        case 901:{//续租时间未开始
            ragex = /(orderCarExplain|usrDays|saveDays|spareCarUsrDays|returnType|timeReturn|timeApplyReturn|swopReason|timeSwop|timeApplySwop|timeTransferEstimated|overdueTransferDays|timeTransferActual|recallReason|timeRecall|refuseReason|timeAudit|timeCreate)/;
            break;
        }
    }
    $('th').filter(function() {
        var id = this.attributes['data-field'].value;
        return id != null && id.match(ragex);
    }).css('display','none');
    $('td').filter(function() {
        var id = this.attributes['data-field'].value;
        return id != null && id.match(ragex);
    }).css('display','none');
}