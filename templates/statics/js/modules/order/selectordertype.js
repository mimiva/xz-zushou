$(function () {
    layui.use(['form', 'layedit', 'laydate'], function () {
        layui.form.render();
    });

    layui.form.on('radio(orderType)', function (data) {
        vm.orderType = data.value;
    });
});

var callback;
var vm = new Vue({
    el:'#rrapp',
    data:{
        orderType:null
},
created: function(){
    //跳转类型赋值不同的callback
    if(parent.layui.larryElem === undefined){
        var _this = this;
        var param = parent.layer.boxParams.boxParams;
        callback = param.callback;
    }else {
        var params = parent.layui.larryElem.boxParams;
        callback = params.callback;
    }
},
updated: function(){
    layui.form.render();
},
methods: {
    closePage:function () {
        // closePage();
        closeCurrent();
    },
    selected:function () {
        if (vm.orderType == null || vm.orderType == ''){
            alert('请选择下单类型');
            return;
        }
        callback(vm.orderType);
        // closePage();
        closeCurrent();
    }
}
});

function closePage() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}
