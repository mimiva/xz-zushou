$(function () {
    /***
     * 页面初始化根据车辆id赋值
     */
    if(parent.layui.larryElem != undefined){
        var params = parent.layui.larryElem.boxParams;
        vm.getCarBasicInforByCarNo(params.carId);
    }
    // 获取交强险、商业险付款对象
    $.ajax({
        type: "POST",
        url: baseURL + "sys/dict/getInfoByType/"+"insurancePaymentTarget",
        contentType: "application/json",
        data:null,
        success: function(r){
            vm.inspectionPayIds= r.dict;
        }
    })

    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form,
            layer = layui.layer,
            layedit = layui.layedit,
            laydate = layui.laydate;

        var upload = layui.upload;
        form.render();
    });

    // 年检附件上传
    layui.use('upload', function () {
        layui.upload.render({
            elem: '#addDeliveryFile',
            url: baseURL + 'file/uploadFile',
            data: {'path': 'inspection'},
            field: 'files',
            auto: true,
            size: 50 * 1024 * 1024,
            accept: 'file', //普通文件
            acceptMime: '.pdf,.doc,.docx,.exl,.xlsx,.jpg,.png,.jpeg,.zip,.rar',
            exts: 'pdf|doc|docx|exl|xlsx|jpg|png|jpeg|zip|rar',
            multiple: true,
            number:20,
            choose: function (obj) {
                obj.preview(function (index, file, result) {
                    var fileName = file.name;
                    var extIndex = fileName.lastIndexOf('.');
                    var ext = fileName.slice(extIndex);
                    var fileNameNotext = fileName.slice(0, extIndex);
                    var regExt = /png|jpg|jpeg/;
                    var fileType = regExt.test(ext) ? 1 : 0;
                    deliveryFileIdTmp = vm.deliveryFileLst.length + '_' + uuid(60);
                    var fileTmp = {
                        id: deliveryFileIdTmp,
                        nameDesc: '年检附件',
                        nameAccessory: fileNameNotext,
                        nameFile: fileName,
                        nameExt: ext,
                        typeFile: fileType,
                    };
                    vm.deliveryFileLst.push(fileTmp);
                });
            },
            done: function (res) {
                if (res.code == '0') {
                    vm.deliveryFileLst.forEach(function (value) {
                        if (value.id === deliveryFileIdTmp) value.url = res.data[0];
                    });
                    vm.deliveryFileLstId = 'deliveryFileLstId_' + uuid(6);
                } else {
                    layer.msg('上传失败', {icon: 5});
                    vm.delDeliveryFile(deliveryFileIdTmp);
                }
                deliveryFileIdTmp = null;
            },
            error: function () {
                layer.msg('上传失败', {icon: 5});
                vm.delDeliveryFile(fileIdTmp);
                deliveryFileIdTmp = null;
            }
        });
    });


    layui.use('laydate', function(){
        var laydate = layui.laydate;

        // 本次年检时间
        laydate.render({
            elem: '#thisTimeInspectionTime',
            type: 'date',
            trigger: 'click',
            done: function (value, date, endDate) {
                vm.inspectionManage.thisTimeInspectionTime = value
                vm.inspectionManage.inspectionYear = date.year;
                $("#inspectionYear").val(date.year);
            }
        });
        // 下次年检时间
        laydate.render({
            elem: '#nextInspectionTime',
            type: 'date',
            trigger: 'click',
            done: function (value) {
                vm.inspectionManage.nextInspectionTime = value;
            }
        });

    });


    layui.form.on('submit(submitEditData)', function(){
        vm.saveOrUpdate();
        return false;
    });

});

var vm = new Vue({
    el: '#rrapp',
    data: {
        inspectionManage:{},
        deliveryFileLst: [],
        carInforData:{},
        deliveryFileLstId: '0',
        carNoAndVinNoDiv:true,
        carNoDiv:true,
        edithidden: true,
        inspectionPayIds:[]
    },
    created: function(){
    },
    computed:{
    },
    updated: function () {
        layui.form.render();
    },
    methods: {
        delDeliveryFile: function (id) {
            for (var i = 0; i < vm.deliveryFileLst.length; i++) {
                if (vm.deliveryFileLst[i].id === id) {
                    vm.deliveryFileLst.splice(i, 1);
                    i = i - 1;
                }
            }
        },
        //选择车牌号
        selectCarNo:function(){
            var index = layer.open({
                title: "选择车辆",
                type: 2,
                content: tabBaseURL + "modules/common/selectcar.html",
                end: function(){
                    var carId=vm.carInforData.carId;
                    if(carId!=null && carId!='' && carId!=undefined){
                        vm.getCarBasicInforByCarNo(carId);
                    }
                }
            });
            layer.full(index);

        },
        //选择车架号
        selectVinNo:function(){
            var index = layer.open({
                title: "选择车辆",
                type: 2,
                content: tabBaseURL + "modules/common/selectcar.html",
                end: function(){
                    var vinNo=vm.carInforData.vinNo;
                    if(vinNo!=null && vinNo!=''&& vinNo!=undefined){
                        vm.getCarBasicInforByVinNo(vinNo);
                    }
                }
            });
            layer.full(index);

        },
        //保存修改方法
        saveOrUpdate: function (event) {
            var isNum = /^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/;
            if (null != vm.inspectionManage.inspectionAmount && vm.inspectionManage.inspectionAmount != undefined && vm.inspectionManage.inspectionAmount != '') {
                if (!isNum.test(vm.inspectionManage.inspectionAmount)) {
                    layer.msg('填写金额错误', {icon: 5});
                    return;
                }
            }
            vm.inspectionManage.deliveryFileLst = vm.deliveryFileLst;
            var url = vm.inspectionManage.id == null ? "maintenance/inspectionmanage/save" : "maintenance/inspectionmanage/update";
            vm.inspectionManage.flag = vm.flag;
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.inspectionManage),
                success: function (r) {
                    if (r.code === 0) {
                        alert('操作成功', function (index) {
                            /*parent.layer.closeAll();
                            parent.vm.reload();*/
                            //关闭操作
                            closeCurrent();
                        });
                    } else {
                        alert(r.msg);
                    }
                }
            });
        },



        deptTree: function(){
            var index = layer.open({
                title: "选择组织机构",
                type: 2,
                area: ['80%', '80%'],
                content: tabBaseURL + "modules/common/selectdeptcommon.html",
                end: function(){
                    layer.close(index);
                }
            });
        },
        zTreeClick: function(event, treeId, treeNode){
            Vue.set(vm.tCarBasic,"deptId",treeNode.deptId);
            Vue.set(vm.tCarBasic,"deptName",treeNode.name);
            layer.closeAll();
        },
        query: function () {
            vm.reload();
        },
        cancel: function(){
            closeCurrent();
        },
        //根据车牌号查询基本信息
        getCarBasicInforByCarNo:function (carId) {
            $.ajax({
                type: "POST",
                url: baseURL + "car/tcarbasic/getCarBasicInforByCarNo/" + carId,
                contentType: "application/json",
                data: {},
                success: function (r) {
                    console.log(r.baseInfor);
                    vm.vehicleInfo = r.baseInfor;
                    vm.inspectionManage.vehicleOrderNo = r.baseInfor.carOrderNo;
                    vm.inspectionManage.carId = r.baseInfor.carId;
                    addVehicleInfo(vm.vehicleInfo);
                    layui.form.render("select");
                }
            });
        },
        //根据车架号查询基本信息
        getCarBasicInforByVinNo:function (vinNo) {
            $.ajax({
                type: "POST",
                url: baseURL + "car/tcarbasic/getCarBasicInforByVinNo/" + vinNo,
                contentType: "application/json",
                data: {},
                success: function (r) {
                    if (r.baseInfor != null) {
                        console.log(r.baseInfor);
                        vm.vehicleInfo = r.baseInfor;
                        vm.inspectionManage.vehicleOrderNo = r.baseInfor.carOrderNo;
                        vm.inspectionManage.carId = r.baseInfor.carId;
                        addVehicleInfo(vm.vehicleInfo);
                        layui.form.render("select");
                    } else {
                        alert("该车架号暂无车辆信息!");
                        return;
                    }
                }
            });
        },

    }
})

/**
 * 自动识别车辆信息
 * @param item
 */
function addVehicleInfo(item) {
    vm.$set(vm.inspectionManage, 'carNo', item.carNo);
    vm.$set(vm.inspectionManage, 'vinNo', item.vinNo);
    //设置车辆品牌/车型
    vm.$set(vm.inspectionManage, 'brandAndCarModel', item.brandName + "/" + item.modelName);
    vm.$set(vm.inspectionManage, 'customerId', item.customerId);
    //客户名称
    vm.$set(vm.inspectionManage, 'customerName', item.customerName);
    //车辆状态
    vm.$set(vm.inspectionManage, 'vehicleStatusShow', item.carStatusStr);
    vm.inspectionManage.vehicleStatus = item.carStatus;
    //车辆订单号
    vm.$set(vm.inspectionManage, 'vehicleOrderNo', item.carOrderNo);
    //车辆所属公司
    //  vm.$set(vm.inspectionManage, 'company', item.belongCompanyName);
    vm.$set(vm.inspectionManage, 'company', item.deptName);
    //租赁开始时间
    vm.$set(vm.inspectionManage, 'rentStartTime', item.timeStartRent);
    //租赁结束时间
    vm.$set(vm.inspectionManage, 'rentEndTime', item.timeFinishRent);
    //车辆所在城市
    vm.$set(vm.inspectionManage, 'city', item.cityName);
    //仓库id
    vm.$set(vm.inspectionManage, 'depotId', item.carDepotId);
    //仓库名称
    vm.$set(vm.inspectionManage, 'depotName', item.carDepotName);
    //车辆用途
    vm.$set(vm.inspectionManage, 'carPurposeShow', item.rentTypeStr);
    //车辆所有人
    vm.$set(vm.inspectionManage, 'carOwner', item.carOwner);
    vm.inspectionManage.carPurpose = item.rentType;
    vm.inspectionManage.carId = item.carId;
}
