$(function () {
        /***
         * 页面初始化根据车辆id赋值
         */
        if(parent.layui.larryElem != undefined){
            var params = parent.layui.larryElem.boxParams;
            vm.getCarBasicInforByCarNo(params.carId);
        }
        vm.getInsuranceAmountStatus();
        vm.getPayObjectData();
        gridTable = layui.table.render({
            id: "insuranceTypeGrid",
            elem: '#insuranceTypeGrid',
            minWidth: 150,
            cols: [[
                {title:'操作', templet:'#barTpl',fixed:"left",align:"center"},
                {field:'commercialInsuranceName', title: '商业险种'},
                {field:'amountInsured',title: '保额/万元'},
                {field:'insuranceExpenses', title: '保险费/元'}
            ]],
            page: false,
            loading: false,
            limit: 500,
            done: function(res, curr, count){
                $('div[lay-id="insuranceTypeGrid"]>div[class="layui-table-box"]>div>table').addClass('table-empty-left');
            }
        });


        layui.use(['form', 'layedit', 'laydate'], function(){
            var form = layui.form,
                layer = layui.layer,
                layedit = layui.layedit,
                laydate = layui.laydate;

            var upload = layui.upload;

                form.verify({
                    carNoVerify: function (value) {
                        if((vm.insuranceManage.carNo =="" || vm.insuranceManage.carNo==null) && (vm.insuranceManage.vinNo =="" || vm.insuranceManage.vinNo==null)){
                            return '请通过车牌号选择或者通过车架号选择车辆信息！';
                        }
                    },
                    vinNoVerify: function (value) {
                        if((vm.insuranceManage.carNo =="" || vm.insuranceManage.carNo==null) && (vm.insuranceManage.vinNo =="" || vm.insuranceManage.vinNo==null)){
                            return '请通过车牌号选择或者通过车架号选择车辆信息！';
                        }
                    },
                    compulsoryAmountVerify: function (value) {
                        var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                        if(value==''){
                            return '';
                        } else {
                            if(!reg.test(value)){
                                return '交强险金额输入格式不正确，请确认!';
                            }
                        }
                    },


                });

            //交强险保险公司
            form.on('select(compulsoryCompanyId)', function (data) {
                vm.insuranceManage.compulsoryCompanyId = data.value;
            });
            //商业保险公司
            form.on('select(commercialCompanyId)', function (data) {
                vm.insuranceManage.commercialCompanyId = data.value;
            });
            //商业险种
            form.on('select(selectInsuranceTypeNo)', function (data) {
                vm.insuranceManage.insuranceTypeNo = data.value;
            });

            // 交强险付款对象
            form.on('select(compulsoryInsurancePayId)', function (data) {
                vm.insuranceManage.compulsoryInsurancePayId = data.value;
                // 获取名称
                $.ajax({
                    type: "POST",
                    url: baseURL + "sys/dict/getSysDictInforByTypeAndCode?type=insurancePaymentTarget&code="+data.value,
                    contentType: "application/json",
                    async: false,
                    data:null,
                    success: function(r){
                        if(r.sysDictEntity!=null){
                            vm.insuranceManage.compulsoryInsurancePayName = r.sysDictEntity.value;
                        }else{
                            vm.insuranceManage.compulsoryInsurancePayName = '';
                        }
                    }
                });
            });
            // 商业险付款对象
            form.on('select(commercialInsurancePayId)', function (data) {
                vm.insuranceManage.commercialInsurancePayId = data.value;
                // 获取名称
                $.ajax({
                    type: "POST",
                    url: baseURL + "sys/dict/getSysDictInforByTypeAndCode?type=insurancePaymentTarget&code="+data.value,
                    contentType: "application/json",
                    data:null,
                    success: function(r){
                        if(r.sysDictEntity!=null){
                            vm.insuranceManage.commercialInsurancePayName = r.sysDictEntity.value;
                        }else{
                            vm.insuranceManage.commercialInsurancePayName = '';
                        }
                    }
                });
            });

            form.render();
        });

        // 交强险照片上传
        layui.use('upload', function () {
            layui.upload.render({
                elem: '#jqxImages',
                url: baseURL + 'file/uploadInsuranceFile',
                data: {'path': 'jqx_images'},
                field: 'files',
                auto: true,
                size: 80 * 1024 * 1024,
                accept: 'file', //普通文件
                acceptMime: '.pdf,.doc,.docx,.exl,.xlsx,.jpg,.png,.jpeg,.zip',
                exts: 'pdf|doc|docx|exl|xlsx|jpg|png|jpeg|zip',
                multiple: true,
                number:20,
                done: function (res) {
                    if (res.code == '0') {
                        res.data.forEach(function (value) {
                            var extIndex = value.resultFilePath.lastIndexOf('.');
                            var ext = value.resultFilePath.slice(extIndex);
                            var fileNameNotext = value.fileName;
                            var regExt = /png|jpg|jpeg/;
                            var fileType = regExt.test(ext) ? 1 : 0;
                            fileIdTmp = vm.jqxFileList.length + '_' + uuid(60);
                            var fileTmp = {
                                id: fileIdTmp,
                                operationId:sessionStorage.getItem("userId"),
                                operationName:sessionStorage.getItem("username"),
                                nameDesc: '交强险照片',
                                nameAccessory: fileNameNotext,
                                nameFile: "交强险文件",
                                nameExt: ext,
                                typeFile: fileType,
                                url: value.resultFilePath
                            };
                            vm.jqxFileList.push(fileTmp);
                            vm.fileLstId = 'fileLstId_' + uuid(6);
                        });
                    } else {
                        layer.msg('上传失败', {icon: 5});
                        vm.delJqxFile(fileIdTmp);
                    }
                    fileIdTmp = null;
                },
                error: function () {
                    layer.msg('上传失败', {icon: 5});
                    vm.delJqxFile(fileIdTmp);
                    fileIdTmp = null;
                }
            });
        });

        //商业险照片上传
        layui.use('upload', function () {
            layui.upload.render({
                elem: '#syxImages',
                url: baseURL + 'file/uploadInsuranceFile',
                data: {'path': 'syx_images'},
                field: 'files',
                auto: true,
                size: 80 * 1024 * 1024,
                accept: 'file', //普通文件
                acceptMime: '.pdf,.doc,.docx,.exl,.xlsx,.jpg,.png,.jpeg,.zip',
                exts: 'pdf|doc|docx|exl|xlsx|jpg|png|jpeg|zip',
                multiple: true,
                number:20,
                done: function (res) {
                    if (res.code == '0') {
                        res.data.forEach(function (value) {
                            var extIndex = value.resultFilePath.lastIndexOf('.');
                            var ext = value.resultFilePath.slice(extIndex);
                            var fileNameNotext = value.fileName;
                            var regExt = /png|jpg|jpeg/;
                            var fileType = regExt.test(ext) ? 1 : 0;
                            fileIdTmp = vm.syxxFileList.length + '_' + uuid(60);
                            var fileTmp = {
                                id: fileIdTmp,
                                operationId:sessionStorage.getItem("userId"),
                                operationName:sessionStorage.getItem("username"),
                                nameDesc: '商业险照片',
                                nameAccessory: fileNameNotext,
                                nameFile: "商业险文件",
                                nameExt: ext,
                                typeFile: fileType,
                                url: value.resultFilePath
                            };
                            vm.syxxFileList.push(fileTmp);
                            vm.fileLstId = 'fileLstId_' + uuid(6);
                        });
                    } else {
                        layer.msg('上传失败', {icon: 5});
                        vm.delSyxFile(fileIdTmp);
                    }
                    fileIdTmp = null;
                },
                error: function () {
                    layer.msg('上传失败', {icon: 5});
                    vm.delSyxFile(fileIdTmp);
                    fileIdTmp = null;
                }
            });
        });

        layui.use('laydate', function(){
        var laydate = layui.laydate;

        //交强险开始结束时间
            var compulsoryStartTime =laydate.render({
            elem: '#compulsoryStartTime',
                trigger: 'click',
            done: function (value, date, endDate) {
                vm.insuranceManage.compulsoryStartTime = value;
                var month = date.month -1;
                compulsoryEndTime.config.min = date;
                compulsoryEndTime.config.min.month = month;
            }
        });
        //date = vm.insuranceManage.compulsoryStartTime;
        //交强险结束时间
            var compulsoryEndTime = laydate.render({
            elem: '#compulsoryEndTime',
                trigger: 'click',
            //range:true,
            done: function (value, date, endDate) {
                vm.insuranceManage.compulsoryEndTime = value;
                var month = date.month -1;
                compulsoryStartTime.config.max = date;
                compulsoryStartTime.config.max.month = month;
            }
        });
        //商业险开始时间
        var commercialStartTime = laydate.render({
            elem: '#commercialStartTime',
            trigger: 'click',
            done: function (value, date, endDate) {
                vm.insuranceManage.commercialStartTime=value;
                var month = date.month -1;
                commercialEndTime.config.min = date;
                commercialEndTime.config.min.month = month;
            }
        });

        //商业险结束时间
        var commercialEndTime = laydate.render({
            elem: '#commercialEndTime',
            trigger: 'click',
            done: function (value, date, endDate) {
                vm.insuranceManage.commercialEndTime=value;
                var month = date.month -1;
                commercialStartTime.config.max = date;
                commercialStartTime.config.max.month = month;
            }
        });
    });


    layui.form.on('submit(saveOrUpdate)', function(){
        vm.saveOrUpdate();
        return false;
    });
    layui.table.on('tool(insuranceTypeGrid)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'edit') {
            vm.insuranceTypeUpdate(data);
        } else if (layEvent === 'del') {
            vm.insuranceTypeDel(data);
        }
    });


});

var vm = new Vue({
    el: '#rrapp',
    data: {
        insuranceTypeGridMS:false,
        //交强险保险公司下拉列表数据源
        compulsoryInsuranceList:[],
        //商业险保险公司下拉列表数据源
        commercialInsuranceList:[],
        // 交强险付款对象数据源
        compulsoryInsurancePay:[],
        // 商业险付款对象数据源
        commercialInsurancePay:[],
        //险种集合
        insuranceTypeList:[],
        //保险单数据源
        insuranceManage:{},
        //商业险种列表数据源
        commercialInsuranceTableList:[],
        jqxFileList: [],
        syxxFileList: [],
        carInforData:{},
        fileLstId: '0',
        bpmChartDtoList:[],
        openFlow:false
    },
    created: function(){
        var _this = this;
        //初始化加载保险公司下拉列表
        $.ajaxSettings.async = false;
        //初始化加载保险公司下拉列表
        $.get(baseURL + "activity/bpmInitChart",{processKey:"carInsuranceApprove"}, function (r) {
            if (r.code == 0){
                _this.bpmChartDtoList = r.bpmInitChart;
                _this.openFlow = r.openFlow;
            }
        });
        //初始化加载保险公司下拉列表
        $.get(baseURL + "insurancecompany/sysinsurancecompany/getInsuranceCompanyList",function(r){
                 
               //交强险
                _this.compulsoryInsuranceList= r.compulsoryInsuranceList;
              //商业险
                _this.commercialInsuranceList= r.commercialInsuranceList;
        });
       //获取险种类型
        $.get(baseURL + "sys/dict/getInfoByType/insuranceType",function(r){
                //险种集合
                _this.insuranceTypeList= r.dict;
        });
        $.ajaxSettings.async = true;
    },
    computed:{
        brandNameAndModelName:{
            get:function() {
                if(this.insuranceManage.brandName!=null  && this.insuranceManage.modelName !=null){
                    return this.insuranceManage.brandName+"/"+this.insuranceManage.modelName;
                }else if(this.insuranceManage.brandName!=null  && this.insuranceManage.modelName ==null){
                    return this.insuranceManage.brandName
                }else if(this.insuranceManage.brandName==null   && this.insuranceManage.modelName !=null){
                    return this.insuranceManage.modelName
                }else {
                    return "--";
                }
            }
        }
    },
    updated: function () {
        layui.form.render();
    },
    methods: {
        delJqxFile: function (id) {
            for(var i = 0 ;i<vm.jqxFileList.length;i++) {
                if(vm.jqxFileList[i].id === id) {
                    vm.jqxFileList.splice(i,1);
                    i= i-1;
                }
            }
        },
        delSyxFile: function (id) {
            for(var i = 0 ;i<vm.syxxFileList.length;i++) {
                if(vm.syxxFileList[i].id === id) {
                    vm.syxxFileList.splice(i,1);
                    i= i-1;
                }
            }
        },
        confirmSup: function(data){
        },

        getPayObjectData(){
            // 获取交强险、商业险付款对象
            $.ajax({
                type: "POST",
                url: baseURL + "sys/dict/getInfoByType/"+"insurancePaymentTarget",
                contentType: "application/json",
                data:null,
                success: function(r){
                    vm.compulsoryInsurancePay= r.dict;
                    vm.commercialInsurancePay= r.dict;
                }
            });
        },

        //选择车牌号
        selectCarNo:function(){
            var index = layer.open({
                title: "选择车辆",
                type: 2,
                content: tabBaseURL + "modules/common/selectcar.html",
                success: function (layero, num) {
                    var iframe = window['layui-layer-iframe' + num];
                    iframe.vm.initType('insurancemanage');
                },
                end: function(){
                    var carId=vm.carInforData.carId;
                    if(carId!=null && carId!='' && carId!=undefined){
                        vm.getCarBasicInforByCarNo(carId);
                    }
                }
            });
            layer.full(index);

        },
        //选择车架号
        selectVinNo:function(){
            var index = layer.open({
                title: "选择车辆",
                type: 2,
                content: tabBaseURL + "modules/common/selectcar.html",
                success: function (layero, num) {
                    var iframe = window['layui-layer-iframe' + num];
                    iframe.vm.initType('insurancemanage');
                },
                end: function(){
                    var vinNo=vm.carInforData.vinNo;
                    if(vinNo!=null && vinNo!=''&& vinNo!=undefined){
                        vm.getCarBasicInforByVinNo(vinNo);
                    }
                }
            });
            layer.full(index);

        },
        //保存修改方法
        saveOrUpdate: function (event) {
           /* var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
            var compulsoryAmount = vm.insuranceManage.compulsoryAmount;
            if(!reg.test(compulsoryAmount)){
                alert('交强险金额的输入格式不正确,请确认!');
                return false;
            }*/

            //商业险种记录
            vm.insuranceManage.commercialInsuranceRecordList = vm.commercialInsuranceTableList;
            vm.insuranceManage.jqxFileList = vm.jqxFileList;
            vm.insuranceManage.syxxFileList = vm.syxxFileList;
            var url = vm.insuranceManage.insuranceManageId == null ? "maintenance/insurancemanage/save" : "maintenance/insurancemanage/update";
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.insuranceManage),
                success: function(r){
                    if(r.code === 0){
                        alert('操作成功', function(index){
                            // parent.layer.closeAll();
                            // parent.vm.reload();
                            //页面关闭
                            closeCurrent();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
        //通过险种编号获取险种名称
        getInsuranceTypeName:function(insuranceTypeNo){
            $.ajax({
                async:false,
                type: "POST",
                url: baseURL + "sys/dict/getSysDictInforByTypeAndCode",
                dataType:"JSON",
                data: {"code":insuranceTypeNo,"type":"insuranceType"},
                success: function(r){
                    if(r.sysDictEntity!=null){
                        var commercialInsuranceName= r.sysDictEntity.value;
                        window.localStorage.setItem("commercialInsuranceName",commercialInsuranceName);
                    }
                }
            });
        },
         //商业险种添加
        addTo:function(){
            var vinNo = $("#vinNo").val();
            var carNo = $("#carNo").val();
            var carId = $("#carId").val();
            if((carNo==null || carNo=="") && (vinNo==null || vinNo=="")){
                alert("请先选择车牌号或者车架号");
                return;
            }

            var insuranceTypeNo=vm.insuranceManage.insuranceTypeNo;
            if(insuranceTypeNo==null ||insuranceTypeNo==""){
                alert("请先选择商业险种类型");
                return;
            }else {
                //通过险种编号查询险种名称
                vm.getInsuranceTypeName(insuranceTypeNo);
                window.localStorage.setItem("vinNo",vinNo);
                window.localStorage.setItem("carNo",carNo);
                window.localStorage.setItem("carId",carId);
                window.localStorage.setItem("insuranceTypeNo",insuranceTypeNo);

                var index = layer.open({
                    title: "商业险种编辑",
                    type: 2,
                    area: ['80%', '80%'],
                    content: tabBaseURL + "modules/maintenance/insurancetypeadd.html",
                    end: function(){
                        layer.close(index);
                    }
                });
            }
        },
        //商业险种修改方法
        insuranceTypeUpdate:function(data){
            window.localStorage.setItem("commercialInsurance",data);
            var index = layer.open({
                title: "商业险种编辑",
                type: 2,
                area: ['80%', '60%'],
                content: tabBaseURL + "modules/maintenance/insurancetypeadd.html",
                success: function(layero, index) {
                    var iframe = window['layui-layer-iframe' + index];
                    iframe.sendData(data);
                },
                end: function(){
                    layer.close(index);
                }
            });

        },
        //商业险种删除方法
        insuranceTypeDel:function(data){
            confirm('确定要删除选中的记录？', function(){
                var randomData=data.randomData;
                var parentData=vm.commercialInsuranceTableList;
                for (var i = parentData.length - 1; i >= 0; i--) {
                    if (parentData[i].randomData==randomData) {
                        parentData.splice(i, 1);
                    }
                }
                alert('删除成功', function(index){
                    vm.reloadCommercialInsurance();
                });
            });
        },
        //加载表格数据--子页面调用
        reloadCommercialInsurance: function () {
            //计算保险费
            var list= vm.commercialInsuranceTableList;
            if(list.length>0){
                var array=new Array()
                for (var i = 0; i <list.length ; i++) {
                    array.push(parseFloat(list[i].insuranceExpenses));
                }
                var commercialAmount = sum(array);
                vm.insuranceManage = Object.assign({}, vm.insuranceManage, {commercialTotalAmount:commercialAmount });
                $.ajax({
                    type: "POST",
                    url: baseURL + "maintenance/insurancemanage/getInsuranceAmountStatus/",
                    contentType: "application/json",
                    data: null,
                    success: function(r){
                        if(r.code === 0){
                            vm.insuranceManage.insuranceAmountValue = r.config.paramValue;
                            console.log(vm.insuranceManage.insuranceAmountValue);
                            if(vm.insuranceManage.insuranceAmountValue == '2'){
                                vm.insuranceManage = Object.assign({}, vm.insuranceManage, {commercialAmount:commercialAmount });
                            }
                        }else{
                            alert(r.msg);
                        }
                    }
                });

            } else{
                var commercialAmount = 0;
                vm.insuranceManage.commercialTotalAmount = commercialAmount;
                vm.insuranceManage.commercialAmount = commercialAmount;
            }
            //重新加载表格
            layui.table.reload('insuranceTypeGrid', {
                data: vm.commercialInsuranceTableList
            });
            vm.insuranceTypeGridMS = true;
        },
        //表格数据重新加载方法
        reloadSourceTable: function(){
            layui.table.reload('gridid', {
                data: vm.sourceDataList
            });
        },
        deptTree: function(){
            var index = layer.open({
                title: "选择组织机构",
                type: 2,
                area: ['80%', '80%'],
                content: tabBaseURL + "modules/common/selectdeptcommon.html",
                end: function(){
                    layer.close(index);
                }
            });
        },
        zTreeClick: function(event, treeId, treeNode){
            Vue.set(vm.tCarBasic,"deptId",treeNode.deptId);
            Vue.set(vm.tCarBasic,"deptName",treeNode.name);
            layer.closeAll();
        },
        query: function () {
            vm.reload();
        },
        cancel: function(){
            // parent.layer.closeAll();
            //关闭页面
            closeCurrent();
        },
        //根据车牌号查询基本信息
        getCarBasicInforByCarNo:function (carId) {
            $.ajax({
                type: "POST",
                url: baseURL + "car/tcarbasic/getCarBasicInforByCarNo/"+carId,
                contentType: "application/json",
                data: {},
                success: function(r){
                    //console.log(r.baseInfor.carNo)
                    vm.insuranceManage = r.baseInfor;
                    if(r.baseInfor.carBrandModelName == null){
                        vm.insuranceManage.carBrandModelName = '';
                    }
                    vm.insuranceManage.vehicleUse = r.baseInfor.rentType;
                    vm.insuranceManage.departureNo = r.baseInfor.carOrderNo;
                    vm.insuranceManage.carBelongCompanyName = r.baseInfor.deptName;
                    vm.insuranceManage.carBrandId = r.baseInfor.brandId;
                    vm.insuranceManage.carBrandName = r.baseInfor.brandName;
                    vm.insuranceManage.carModelId = r.baseInfor.modelId;
                    vm.insuranceManage.carModelName = r.baseInfor.modelName;
                    vm.insuranceManage.carSeriesId = r.baseInfor.seriesId;
                    vm.insuranceManage.carSeriesName = r.baseInfor.seriesName;
                    vm.insuranceManage.carOwner = r.baseInfor.carOwner;
                    if(r.baseInfor.timeStartRent != null){
                        vm.insuranceManage.timeStartRent = new Date(r.baseInfor.timeStartRent).format("yyyy-MM-dd");
                    }
                    if(r.baseInfor.timeFinishRent != null){
                        vm.insuranceManage.timeFinishRent = new Date(r.baseInfor.timeFinishRent).format("yyyy-MM-dd");
                    }
                }
            });
        },
        //根据车架号查询基本信息
        getCarBasicInforByVinNo:function (vinNo) {
            $.ajax({
                type: "POST",
                url: baseURL + "car/tcarbasic/getCarBasicInforByVinNo/"+vinNo,
                contentType: "application/json",
                data: {},
                success: function(r){
                    vm.insuranceManage = r.baseInfor;
                    if(r.baseInfor.carBrandModelName == null){
                        vm.insuranceManage.carBrandModelName = '';
                    }
                    vm.insuranceManage.vehicleUse = r.baseInfor.rentType;
                    vm.insuranceManage.departureNo = r.baseInfor.carOrderNo;
                    vm.insuranceManage.carBelongCompanyName = r.baseInfor.deptName;
                    vm.insuranceManage.carBrandId = r.baseInfor.brandId;
                    vm.insuranceManage.carBrandName = r.baseInfor.brandName;
                    vm.insuranceManage.carModelId = r.baseInfor.modelId;
                    vm.insuranceManage.carModelName = r.baseInfor.modelName;
                    vm.insuranceManage.carSeriesId = r.baseInfor.seriesId;
                    vm.insuranceManage.carSeriesName = r.baseInfor.seriesName;
                    if(r.baseInfor.timeStartRent != null){
                        vm.insuranceManage.timeStartRent = new Date(r.baseInfor.timeStartRent).format("yyyy-MM-dd");
                    }
                    if(r.baseInfor.timeFinishRent != null){
                        vm.insuranceManage.timeFinishRent = new Date(r.baseInfor.timeFinishRent).format("yyyy-MM-dd");
                    }
                }
            });
        },

        // 获取商业险费用是否支持手动输入系统参数
        getInsuranceAmountStatus: function () {
            $.ajax({
                type: "POST",
                url: baseURL + "maintenance/insurancemanage/getInsuranceAmountStatus/",
                contentType: "application/json",
                data: null,
                success: function(r){
                    if(r.code === 0){
                        vm.insuranceManage.insuranceAmountValue = r.config.paramValue;
                        console.log(vm.insuranceManage.insuranceAmountValue);
                        if(vm.insuranceManage.insuranceAmountValue == '1'){
                            $("#commercialAmount").removeAttr("readonly");
                        } else{
                            $("#commercialAmount").attr("readonly","readonly");
                        }
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },

    }
})

//求和计算
function sum(arr) {
    var s = 0;
    for (var i=arr.length-1; i>=0; i--) {
        console.log(isValueNaN(arr[i]));
        if(isValueNaN(arr[i])){
            arr[i] = 0;
        }
        s += arr[i];
    }
    return s.toFixed(2);
}

function isValueNaN(value) {
    return typeof value === 'number' && isNaN(value);
}