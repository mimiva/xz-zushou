$(function () {
    layui.config({
        base: '../../statics/common/'
    }).extend({
        soulTable: 'layui/soultable/ext/soulTable.slim'
    });
    layui.use(['form', 'layedit', 'laydate', 'element', 'table', 'soulTable'], function(){
        init(layui);
        layui.form.render();
    });

});
var vm = new Vue({
    el: '#rrapp',
    data: {
        q: {
            carNo: null,
        },
        carGpsInfor:{},
    },
    computed: function () {},
    updated: function () {
        layui.form.render();
    },
    methods: {
        selectedRows: function () {
            var list = layui.table.checkStatus('gridid').data;
            if(list.length == 0){
                alert("请选择一条记录");
                return ;
            }
            var ids = [];
            $.each(list, function(index, item) {
                ids.push(item);
            });
            return ids;
        },
        reset: function () {
            resetNULL(vm.q);
        },
        query: function () {
            vm.reload();
        },
        cancel:function(){
            var index = parent.layer.getFrameIndex(window.name);
            parent.layer.close(index);
        },
        save:function(){
            var deviceInfor = JSON.parse(window.localStorage.getItem("deviceInfor"));
            var list = layui.table.checkStatus('gridid').data;
            if(list.length == 0){
                alert("请选择一条记录");
                return ;
            }
            vm.carGpsInfor.carBasicId=list[0].carBasicId;
            vm.carGpsInfor.carId=list[0].carId;
            vm.carGpsInfor.carPlateNo=list[0].carNo;
            if(deviceInfor!=null){
                vm.carGpsInfor.deviceId=deviceInfor.deviceId;
                vm.carGpsInfor.deviceManufacturerNumber=deviceInfor.manufacturerNumber;
                vm.carGpsInfor.deviceNo=deviceInfor.deviceNo;
            }
            confirm('确定要绑定该车辆？', function(){
                $.ajax({
                    type: "POST",
                    url: baseURL + "car/gps/bundlingCar",
                    contentType: "application/json",
                    data: JSON.stringify(vm.carGpsInfor),
                    success: function(r){
                        if(r.code == 0){
                            alert('操作成功', function(index){
                                var index = parent.layer.getFrameIndex(window.name);
                                parent.layer.close(index);
                                parent.vm.reload();
                            });
                        }else{
                            alert(r.msg);
                        }
                    }
                });
            });

             /*//父页面调用时需创建 carInforData 数据源
             parent.vm.tCarBasic = Object.assign({}, parent.vm.tCarBasic, {
                deviceId: list[0].deviceId,
                gpsDeviceNo: list[0].deviceNo,
                manufacturerNumber: list[0].manufacturerNumber
             });*/

        },
        reload: function (event) {
            layui.table.reload('gridid', {
                page: {
                    curr: 1
                },
                where: JSON.parse(JSON.stringify(vm.q))
            });
        }
    }
});


function init(layui) {
    initTable(layui.table, layui.soulTable);
    initData();
    initChecked(layui.form);
}

function initChecked(form) {
    //设备供应商查询
    form.on('select(manufacturerNumber)', function (data) {
        vm.q.manufacturerNumber = data.value;
    });

}
function initTable(table, soulTable) {
    table.render({
        id: "gridid",
        elem: '#grid',
        url: baseURL + 'deviceinformation/deviceinformation/getUnboundCarInfor',
        where: JSON.parse(JSON.stringify(vm.q)),
        cols: [[
            {type:'radio'},
            {field:'carNo', title: '车牌号', minWidth:200, templet: function (d) {return isEmpty(d.carNo);}},
            {field:'brandName', title: '品牌名称', minWidth:200, templet: function (d) {return isEmpty(d.brandName);}},
            {field:'modelName', title: '车型名称', minWidth:200, templet: function (d) {return isEmpty(d.modelName);}},
            {field:'seriesName', title: '车系名称', minWidth:200, templet: function (d) {return isEmpty(d.seriesName);}},
        ]],
        page: true,
        loading: true,
        limits: [10,20, 50, 100],
        limit: 10,
        autoColumnWidth: {
            init: false
        },
        done: function(res, curr, count){
            soulTable.render(this);
            $('div[lay-id="gridid"]>div[class="layui-table-box"]>div>table').addClass('table-empty-left');
            $(".layui-table-main tr").each(function (index, val) {
                $($(".layui-table-fixed-l .layui-table-body tbody tr")[index]).height($(val).height());
                $($(".layui-table-fixed-r .layui-table-body tbody tr")[index]).height($(val).height());
            });
        }
    });
    initTableEvent(table);
}

function initTableEvent(table) {
    table.on('tool(grid)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (obj.event === 'selectCarInfor') {
            vm.selectCarInfor(data);
        }
    });
}

function initData() {
    //初始化查询数据字典-设备生产商
    $.ajax({
        type: "POST",
        url: baseURL + "sys/dict/getInfoByType/"+"gpsEquipmentSupplier",
        contentType: "application/json",
        data:null,
        success: function(r){
            vm.gpsEquipmentSupplier = r.dict;
        }
    });
}