$(function () {
    layui.config({
        base: '../../statics/common/'
    }).extend({
        soulTable: 'layui/soultable/ext/soulTable.slim'
    });
    layui.use(['form', 'layedit', 'laydate', 'element', 'table', 'soulTable'], function(){
        init(layui);
        layui.form.render();
    });
});

var vm = new Vue({
    el:'#rrapp',
    data:{
        deviceInformation: {}
    },
    created: function(){
        var _this = this;
        var param = parent.layer.boxParams.boxParams;
        _this.deviceInformation = param.data;
    },
    computed:{
        powerFlag: {
            get: function () {
                if (this.deviceInformation.powerFlag == 0) {
                    return "是";
                } else if (this.deviceInformation.powerFlag == 1) {
                    return "否";
                } else {
                    return "--";
                }
            }
        },
        getDeviceType: {
            get: function () {
                if (this.deviceInformation.deviceKind == 0) {
                    return "无线设备";
                } else if (this.deviceInformation.deviceKind == 1) {
                    return "有线设备";
                } else {
                    return "--";
                }
            }

        }

    },
    updated: function(){
        layui.form.render();
    },
    methods: {

    }
});

function init(layui) {
    initEventListener(layui);
}

function initEventListener(layui) {
    initClick();
}

function initClick() {
    $("#closePage").on('click', function(){
        closePage();
    });
}

function closePage() {
    parent.vm.isClose = true;
    var index = parent.layer.getFrameIndex(window.name);
    parent.vm.reload();
    parent.layer.close(index);
}
