$(function () {
    vm.ouinsuranceOrder={};
    vm.getOutinsuranceOrderInfor(window.localStorage.getItem("outInsuranceOrderId"));

    layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form;
        layer = layui.layer,
            layedit = layui.layedit,
            laydate = layui.laydate;
        form.verify({
            /*receiveDateVerify: function (value) {
                if (value == "" || value == null) {
                    return '接收时间不能为空';
                }
            },
            outDateVerify: function (value) {
                if (value == "" || value == null) {
                    return '出险时间不能为空';
                }
            },
            outAddrVerify: function (value) {
                if (value == "" || value == null) {
                    return '出险地点不能为空';
                }
            },
            reporterVerify: function (value) {
                if (value == "" || value == null) {
                    return '报案人不能为空';
                }
            },
            outReasonVerify: function (value) {
                if (value == "" || value == null) {
                    return '出险经过及原因不能为空';
                }
            },
            outReasonVerify: function (value) {
                if (value == "" || value == null) {
                    return '出险经过及原因不能为空';
                }
            },
            insuranceCompanyVerify: function (value) {
                if (value == "" || value == null) {
                    return '保险公司不能为空';
                }
            },
            outLevelVerify: function (value) {
                if (value == "" || value == null) {
                    return '事故等级不能为空';
                }
            },
            sxIsPayVerify: function (value) {
                if (value == "" || value == null) {
                    return '商业险赔付不能为空';
                }

            },*/
            sxPayFeeVerify: function (value) {
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                /*if (value == "" || value == null) {
                    return '赔付金额不能为空';
                }*/
                if(value!=null && value!=""){
                    if(!reg.test(value)){
                        return '金额的输入格式不正确,请确认!';
                    }
                }

            },
            /*sxPayDateVerify: function (value) {
                if (value == "" || value == null) {
                    return '支付时间不能为空';
                }
            },
            qxIsPayVerify: function (value) {
                if (value == "" || value == null) {
                    return '交强险赔付不能为空';
                }
            },*/
            qxPayFeeVerify: function (value) {
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                /*if (value == "" || value == null) {
                    return '赔付金额不能为空';
                }*/
                if(value!=null && value!=""){
                    if(!reg.test(value)){
                        return '金额的输入格式不正确,请确认!';
                    }
                }

            },
            floatFee: function (value) {
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                if(value!=null && value!=""){
                    if(value <=0){
                        return '保险上浮费必须大于0';
                    }
                    if(!reg.test(value)){
                        return '保险上浮费的输入格式不正确,请确认!';
                    }
                }
            },
            repairBackFee: function (value) {
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                if(value!=null && value!=""){
                    if(value <=0){
                        return '维修返佣金额必须大于0';
                    }
                    if(!reg.test(value)){
                        return '维修返佣金额的输入格式不正确,请确认!';
                    }
                }
            },
            /*qxPayDateVerify: function (value) {
                if (value == "" || value == null) {
                    return '支付时间不能为空';
                }
            },
            responsiblePartyVerify: function (value) {
                if (value == "" || value == null) {
                    return '责任方不能为空';
                }
            }*/
        });
        form.render();
    });
    //接收时间
    laydate.render({
        elem: '#receiveDate',
        type:'date',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.ouinsuranceOrder.receiveDate = value;
        }
    });
    //出险时间
    laydate.render({
        elem: '#outDate',
        type:'date',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.ouinsuranceOrder.outDate = value;
        }
    });

    //商业险赔付保险公司
    layui.form.on('select(sxInsuranceCompanyId)', function (data) {
        vm.ouinsuranceOrder.sxInsuranceCompanyId = data.value;
    });

    //交强险赔付保险公司
    layui.form.on('select(qxInsuranceCompanyId)', function (data) {
        vm.ouinsuranceOrder.qxInsuranceCompanyId = data.value;
    });

    //事故等级
    layui.form.on('select(outLevelFilter)', function (data) {
        vm.ouinsuranceOrder.outLevel = data.value;
    });

    //商业险赔付
    layui.form.on('select(sxIsPayFilter)', function (data) {
        vm.ouinsuranceOrder.sxIsPay = data.value;
    });

    //支付时间
    laydate.render({
        elem: '#sxPayDate',
        type:'date',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.ouinsuranceOrder.sxPayDate = value;
        }
    });

    //交强险赔付
    layui.form.on('select(qxIsPayFilter)', function (data) {
        vm.ouinsuranceOrder.qxIsPay = data.value;
    });

    //强险支付时间
    laydate.render({
        elem: '#qxPayDate',
        type:'date',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.ouinsuranceOrder.qxPayDate = value;
        }
    });

    //责任方
    layui.form.on('select(responsiblePartyFilter)', function (data) {
        vm.ouinsuranceOrder.responsibleParty = data.value;
    });

    //附件上传
    layui.upload.render({
        elem: '#addFile',
        url: baseURL + 'file/uploadInsuranceFile',
        data: {'path':'outinsuranceorder'},
        field:'files',
        auto:true,
        size: 50*1024*1024,
        accept: 'file', //普通文件
        acceptMime: '.pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.jpeg,.zip,.rar',
        exts: 'pdf|doc|docx|xls|xlsx|jpg|png|jpeg|zip|rar', //
        multiple: true,
        number:20,
        done: function (res) {
            RemoveLoading();
            if (res.code != '0') {
                layer.msg('上传失败', {icon: 5});
                vm.delFile(fileIdTmp);
                fileIdTmp = null;
                return false;
            }
            res.data.forEach(function (value) {
                var extIndex = value.resultFilePath.lastIndexOf('.');
                var ext = value.resultFilePath.slice(extIndex);
                var fileNameNotext = value.fileName;
                var regExt = /png|jpg|jpeg/;
                var fileType = regExt.test(ext) ? 1 : 0;
                fileIdTmp = vm.fileLst.length + '_' + uuid(60);
                var fileTmp = {
                    id: fileIdTmp,
                    operationId:sessionStorage.getItem("userId"),
                    operationName:sessionStorage.getItem("username"),
                    nameDesc: '出险附件',
                    nameAccessory: fileNameNotext,
                    nameFile: fileNameNotext,
                    nameExt: ext,
                    typeFile: fileType,
                    url: value.resultFilePath
                };
                vm.fileLst.push(fileTmp);
                vm.fileLstId = 'fileLstId_' + uuid(6);
            });
            fileIdTmp = null;
        },
        error: function () {
            RemoveLoading();
            layer.msg('上传失败', {icon: 5});
            vm.delFile(fileIdTmp);
            fileIdTmp = null;
        }
    });
    layui.upload.render({
        elem: '#addFile2',
        url: baseURL + 'file/uploadFile',
        data: {'path':'processApprove'},
        field:'files',
        auto:true,
        size: 50*1024*1024,
        accept: 'file', //普通文件
        acceptMime: '.pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.jpeg,.zip,.rar',
        exts: 'pdf|doc|docx|xls|xlsx|jpg|png|jpeg|zip|rar', //
        multiple: true,
        number:20,
        choose: function(obj){
            // PageLoading();
            obj.preview(function(index, file, result){
                var fileName = file.name;
                var extIndex = fileName.lastIndexOf('.');
                var ext = fileName.slice(extIndex);
                var fileNameNotext = fileName.slice(0, extIndex);
                var regExt = /png|jpg|jpeg/;
                var fileType = regExt.test(ext) ? 1:0;
                fileIdTmp = vm.fileLst2.length + '_' + uuid(60);
                var fileTmp={
                    id: fileIdTmp,
                    operationId:sessionStorage.getItem("userId"),
                    operationName:sessionStorage.getItem("username"),
                    nameDesc:'审批附件',
                    nameAccessory:fileNameNotext,
                    nameFile:fileName,
                    nameExt:ext,
                    typeFile:fileType,
                };
                vm.fileLst2.push(fileTmp);
            });
        },
        done: function (res) {
            // RemoveLoading();
            if (res.code == '0') {
                vm.fileLst2.forEach(function (value) {
                    if (value.id === fileIdTmp) value.url = res.data[0];
                });
                vm.fileLstId2 = 'fileLstId_' + uuid(6);
            } else {
                layer.msg('上传失败', {icon: 5});
                vm.delFile2(fileIdTmp);
            }
            fileIdTmp = null;
        },
        error: function () {
            RemoveLoading();
            layer.msg('上传失败', {icon: 5});
            vm.delFile2(fileIdTmp);
            fileIdTmp = null;
        }
    });

    layui.form.on('submit(saveOrUpdate)', function(){
        vm.saveOrUpdate();
        return false;
    });

});

var vm = new Vue({
    el:'#rrapp',
    data:{
        showForm: false,
        carInforData:{},
        ouinsuranceOrder: {},
        fileLst:[],
        fileLstId: '0',
        compulsoryInsuranceList:[],
        outLevelList:[],
        boxShow: false,
        boxInputShow: false,
        boxTitle: '',
        fileLst2:[],
        fileLstId2: '0',
        boxMark: '',
        boxHolder: '',
        boxTxt: '',
        viewTag:'edit',
        recallNodeName:null,
        processApproveForm:{},
        bpmChartDtoList: [],
        nodeType:null,
        instanceStatus:null
    },
    updated: function(){
        layui.form.render();
    },
    created: function(){
        var _this = this;
        $.ajaxSettings.async = false;
        var param = parent.layer.boxParams.boxParams;
        if(param){
            _this.recallNodeName = param.recallName;
            _this.viewTag = param.viewTag;
            _this.instanceStatus = param.instanceStatus;
            _this.processApproveForm = param;
            //初始化加载保险公司下拉列表
            if(_this.viewTag != 'edit'){
                $.get(baseURL + "activity/bpmChart",{processKey:param.processKey,instanceId:param.instanceId}, function (r) {
                    if (r.code == 0){
                        _this.bpmChartDtoList = r.bpmChart;
                    }
                });
            }
            $.get(baseURL + "activity/getNodeType",{instanceId:param.instanceId},function(r){
                _this.nodeType = r.nodeType;
            });
        }
        $.get(baseURL + "insurancecompany/sysinsurancecompany/getInsuranceCompanyInforList",function(r){
            _this.compulsoryInsuranceList = r.compulsoryInsuranceList;
        });
        $.get(baseURL + "sys/dict/getInfoByType/"+"accidentLevel", function(r){
            _this.outLevelList= r.dict;
        });
        $.ajaxSettings.async = true;
    },
    methods: {
        requestAction: function (action,type) {
            PageLoading();
            vm.processApproveForm.processOperationType = type;
            vm.processApproveForm.businessId = window.localStorage.getItem("outInsuranceOrderId");
            vm.processApproveForm.approveContent = vm.boxTxt;
            vm.processApproveForm.approveFileList=vm.fileLst2;
            var closeBox = false;
            $.ajax({
                type: "POST",
                url: baseURL + "outinsuranceorder/ouinsuranceorder/" + action,
                contentType: "application/json",
                async:false,
                data: JSON.stringify(vm.processApproveForm),
                success: function (r) {
                    RemoveLoading();
                    if (parseInt(r.code) === 0) {
                        alert('操作成功', function () {
                            closeBox = true;
                            layer.closeAll();
                            var index = parent.layer.getFrameIndex(window.name);
                            parent.vm.reload();
                            parent.layer.close(index);
                        });
                    } else {
                        closeBox = false;
                        alert(r.msg);
                    }
                }
            });
            return closeBox;
        },
        showBox: function (submit) {
            vm.boxTxt = '';
            var index = layer.open({
                title: vm.boxTitle,
                type: 1,
                area: ['90%', '95%'],
                btn:['取消','确定'] ,
                content: $("#boxShow"),
                btnAlign: 'c',
                end: function(){
                    vm.boxShow = false;
                    vm.fileLst2=[];
                    layer.close(index);
                },
                btn1:function (index, layero) {
                    vm.boxShow = false;
                    vm.fileLst2=[];
                    layer.close(index);
                },
                btn2:function (index, layero) {
                    return submit();
                }
            });
            vm.boxShow = true;
        },
        approve:function(){
            vm.boxTitle = '审核通过';
            vm.boxMark = '审核反馈';
            vm.boxHolder = '可以输入审核意见/选填';
            vm.boxInputShow = true;
            vm.showBox(function () {
                return vm.requestAction('handle', 4);
            });
        },
        getFileData:function (data){
            let param={
                data:data
            }
            var index = layer.open({
                title: "查看",
                type: 2,
                boxParams: param,
                content: tabBaseURL +"modules/common/approvefileview.html",
                end: function () {
                    layer.close(index);
                }
            });
            layer.full(index);
        },
        delFile2: function (id) {
            for(var i = 0 ;i<vm.fileLst2.length;i++) {
                if(vm.fileLst2[i].id === id) {
                    vm.fileLst2.splice(i,1);
                    i= i-1;
                }
            }
        },
        reject:function(){
            vm.boxTitle = '审核驳回-驳回' + vm.recallNodeName + '节点';
            vm.boxMark = '审核反馈';
            vm.boxHolder = '可以输入审核意见/必填';
            vm.boxInputShow = true;
            vm.showBox(function () {
                if (vm.boxTxt == null || vm.boxTxt == ''){
                    alert('请输入审核意见');
                    return false;
                }
                return vm.requestAction('handle', 3);
            });
        },
        reedit:function(){
            vm.viewTag = 'reedit';
        },
        cancel:function(){
            parent.layer.closeAll();
        },
        //选择车牌号
        selectCarNo:function(){
            var index = layer.open({
                title: "选择车牌号",
                type: 2,
                content: tabBaseURL + "modules/common/selectcar.html",
                end: function(){
                    var carId=vm.carInforData.carId;
                    if(carId!=null && carId!='' && carId!=undefined){
                        vm.getCarBasicInforByCarNo(carId);
                    }
                }
            });
            layer.full(index);

        },
        //选择车架号
        selectVinNo:function(){
            var index = layer.open({
                title: "选择车架号",
                type: 2,
                content: tabBaseURL + "modules/common/selectcar.html",
                end: function(){
                    var vinNo=vm.carInforData.vinNo;
                    if(vinNo!=null && vinNo!=''&& vinNo!=undefined){
                        vm.getCarBasicInforByVinNo(vinNo);
                    }
                }
            });
            layer.full(index);

        },
        //根据车牌号查询基本信息
        getCarBasicInforByCarNo:function (carId) {
            $.ajax({
                type: "POST",
                url: baseURL + "car/tcarbasic/getCarBasicInforByCarNo/"+carId,
                contentType: "application/json",
                data: {},
                success: function(r){
                    vm.ouinsuranceOrder.carId = carId;
                    vm.ouinsuranceOrder.carNo=r.baseInfor.carNo;
                    vm.ouinsuranceOrder.vinNo=r.baseInfor.vinNo;
                    vm.ouinsuranceOrder.carBrandModelName=r.baseInfor.carBrandModelName;
                    vm.ouinsuranceOrder.customerName=r.baseInfor.customerName;
                    vm.ouinsuranceOrder.carStatus=r.baseInfor.carStatus;
                    vm.ouinsuranceOrder.carStatusStr=r.baseInfor.carStatusStr;
                    vm.ouinsuranceOrder.carOrderNo=r.baseInfor.carOrderNo;
                    vm.ouinsuranceOrder.belongCompanyName=r.baseInfor.belongCompanyName;
                    vm.ouinsuranceOrder.timeStartRent=r.baseInfor.timeStartRent;
                    vm.ouinsuranceOrder.timeFinishRent=r.baseInfor.timeFinishRent;
                    vm.ouinsuranceOrder.sumOdograph=r.baseInfor.sumOdograph;
                    vm.ouinsuranceOrder.rentTypeStr=r.baseInfor.rentTypeStr;
                    vm.ouinsuranceOrder.cityName=r.baseInfor.cityName;
                    vm.ouinsuranceOrder.depotName=r.baseInfor.carDepotName;
                    vm.ouinsuranceOrder.depotId=r.baseInfor.carDepotId;
                }
            });
        },
        //根据车架号查询基本信息
        getCarBasicInforByVinNo:function (vinNo) {
            $.ajax({
                type: "POST",
                url: baseURL + "car/tcarbasic/getCarBasicInforByVinNo/"+vinNo,
                contentType: "application/json",
                data: {},
                success: function(r){
                    vm.ouinsuranceOrder.carId = r.baseInfor.carId;
                    vm.ouinsuranceOrder.carNo=r.baseInfor.carNo;
                    vm.ouinsuranceOrder.vinNo=r.baseInfor.vinNo;
                    vm.ouinsuranceOrder.carBrandModelName=r.baseInfor.carBrandModelName;
                    vm.ouinsuranceOrder.customerName=r.baseInfor.customerName;
                    vm.ouinsuranceOrder.carStatus=r.baseInfor.carStatus;
                    vm.ouinsuranceOrder.carStatusStr=r.baseInfor.carStatusStr;
                    vm.ouinsuranceOrder.carOrderNo=r.baseInfor.carOrderNo;
                    vm.ouinsuranceOrder.belongCompanyName=r.baseInfor.belongCompanyName;
                    vm.ouinsuranceOrder.timeStartRent=r.baseInfor.timeStartRent;
                    vm.ouinsuranceOrder.timeFinishRent=r.baseInfor.timeFinishRent;
                    vm.ouinsuranceOrder.sumOdograph=r.baseInfor.sumOdograph;
                    vm.ouinsuranceOrder.rentTypeStr=r.baseInfor.rentTypeStr;
                    vm.ouinsuranceOrder.cityName=r.baseInfor.cityName;
                    vm.ouinsuranceOrder.depotName=r.baseInfor.carDepotName;
                    vm.ouinsuranceOrder.depotId=r.baseInfor.carDepotId;
                }
            });
        },
        saveOrUpdate: function (event) {
            vm.ouinsuranceOrder.fileLst=vm.fileLst;
            vm.ouinsuranceOrder.carPlateNo=vm.ouinsuranceOrder.carNo;
            vm.ouinsuranceOrder.customer=vm.ouinsuranceOrder.customerName;
            vm.ouinsuranceOrder.carPurpose=vm.ouinsuranceOrder.rentTypeStr;
            vm.ouinsuranceOrder.viewTag = vm.viewTag;
            vm.ouinsuranceOrder.instanceId = vm.processApproveForm.instanceId;
            var url ="outinsuranceorder/ouinsuranceorder/update";
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.ouinsuranceOrder),
                success: function(r){
                    if(r.code === 0){
                        alert('操作成功', function(index){
                            parent.layer.closeAll();
                            parent.vm.reload();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
        getOutinsuranceOrderInfor:function (id) {
            $.ajax({
                type: "POST",
                url: baseURL + "outinsuranceorder/ouinsuranceorder/info/"+id,
                contentType: "application/json",
                data: null,
                success: function(r){
                    if(r.code === 0){
                        vm.ouinsuranceOrder = Object.assign({}, vm.ouinsuranceOrder, r.outinsuranceOrderDetil);
                        vm.ouinsuranceOrder = Object.assign({}, vm.ouinsuranceOrder, {carNo:r.outinsuranceOrderDetil.carPlateNo});
                        vm.ouinsuranceOrder = Object.assign({}, vm.ouinsuranceOrder, {customerName:r.outinsuranceOrderDetil.customer});
                        vm.fileLst=r.outinsuranceOrderDetil.fileLst;

                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
        delFile: function (id) {
            for(var i = 0 ;i<vm.fileLst.length;i++) {
                if(vm.fileLst[i].id === id) {
                    vm.fileLst.splice(i,1);
                    i= i-1;
                }
            }
        },

    }
});


