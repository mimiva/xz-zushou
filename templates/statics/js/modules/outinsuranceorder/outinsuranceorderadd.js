$(function () {

    /***
     * 页面初始化根据车辆id赋值
     */
    if(parent.layui.larryElem != undefined){
        var params = parent.layui.larryElem.boxParams;
        vm.getCarBasicInforByCarNo(params.carId);
    }

    layui.use(['form', 'layedit', 'laydate'], function () {
        var form = layui.form;
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;

        form.verify({
            carNoVerify: function (value) {
                if((vm.ouinsuranceOrder.carNo =="" || vm.ouinsuranceOrder.carNo==null) && (vm.ouinsuranceOrder.vinNo =="" || vm.ouinsuranceOrder.vinNo==null)){
                    return '请通过车牌号选择或者通过车架号选择车辆信息！';
                }
            },
            vinNoVerify: function (value) {
                if((vm.ouinsuranceOrder.carNo =="" || vm.ouinsuranceOrder.carNo==null) && (vm.ouinsuranceOrder.vinNo =="" || vm.ouinsuranceOrder.vinNo==null)){
                    return '请通过车牌号选择或者通过车架号选择车辆信息！';
                }
            },
            /*receiveDateVerify: function (value) {
                if (value == "" || value == null) {
                    return '接收时间不能为空';
                }
            },
         outDateVerify: function (value) {
            if (value == "" || value == null) {
                return '出险时间不能为空';
            }
        },
         outAddrVerify: function (value) {
            if (value == "" || value == null) {
                return '出险地点不能为空';
            }
        },
         reporterVerify: function (value) {
            if (value == "" || value == null) {
                return '报案人不能为空';
            }
        },
       outReasonVerify: function (value) {
            if (value == "" || value == null) {
                return '出险经过及原因不能为空';
            }
        },
        outReasonVerify: function (value) {
           if (value == "" || value == null) {
               return '出险经过及原因不能为空';
           }
        },
        insuranceCompanyVerify: function (value) {
            if (value == "" || value == null) {
                return '保险公司不能为空';
            }
        },
        outLevelVerify: function (value) {
           if (value == "" || value == null) {
               return '事故等级不能为空';
           }
        },
        sxIsPayVerify: function (value) {
           if (value == "" || value == null) {
               return '商业险赔付不能为空';
           }

        },*/
            sxPayFeeVerify: function (value) {
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                /*if (value == "" || value == null) {
                    return '赔付金额不能为空';
                }*/
                if(value!=null && value!=""){
                    if(!reg.test(value)){
                        return '金额的输入格式不正确,请确认!';
                    }
                }

            },
            /*sxPayDateVerify: function (value) {
              if (value == "" || value == null) {
                  return '支付时间不能为空';
              }
           },
           qxIsPayVerify: function (value) {
              if (value == "" || value == null) {
                  return '交强险赔付不能为空';
              }
           },*/
            qxPayFeeVerify: function (value) {
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                /*if (value == "" || value == null) {
                    return '赔付金额不能为空';
                }*/
                if(value!=null && value!=""){
                    if(!reg.test(value)){
                        return '金额的输入格式不正确,请确认!';
                    }
                }
            },
            floatFee: function (value) {
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                if(value!=null && value!=""){
                    if(value <=0){
                        return '保险上浮费必须大于0';
                    }
                    if(!reg.test(value)){
                        return '保险上浮费的输入格式不正确,请确认!';
                    }
                }
            },
            repairBackFee: function (value) {
                var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
                if(value!=null && value!=""){
                    if(value <=0){
                        return '维修返佣金额必须大于0';
                    }
                    if(!reg.test(value)){
                        return '维修返佣金额的输入格式不正确,请确认!';
                    }
                }
            },
            /*qxPayDateVerify: function (value) {
               if (value == "" || value == null) {
                   return '支付时间不能为空';
               }
            },
            responsiblePartyVerify: function (value) {
               if (value == "" || value == null) {
                   return '责任方不能为空';
               }
            },*/
        });
        form.render();
    });
    //接收时间
    laydate.render({
        elem: '#receiveDate',
        type:'date',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.ouinsuranceOrder.receiveDate = value;
        }
    });
    //出险时间
    laydate.render({
        elem: '#outDate',
        type:'date',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.ouinsuranceOrder.outDate = value;
        }
    });

    //事故等级
    layui.form.on('select(outLevelFilter)', function (data) {
        vm.ouinsuranceOrder.outLevel = data.value;
    });

    //商业险保险公司
    layui.form.on('select(sxInsuranceCompanyId)', function (data) {
        vm.ouinsuranceOrder.sxInsuranceCompanyId = data.value;
    });
    //商业险赔付
    layui.form.on('select(sxIsPayFilter)', function (data) {
        vm.ouinsuranceOrder.sxIsPay = data.value;
    });

    //支付时间
    laydate.render({
        elem: '#sxPayDate',
        type:'date',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.ouinsuranceOrder.sxPayDate = value;
        }
    });

    //交强险保险公司
    layui.form.on('select(qxInsuranceCompanyId)', function (data) {
        vm.ouinsuranceOrder.qxInsuranceCompanyId = data.value;
    });

    //交强险赔付
    layui.form.on('select(qxIsPayFilter)', function (data) {
        vm.ouinsuranceOrder.qxIsPay = data.value;
    });

    //强险支付时间
    laydate.render({
        elem: '#qxPayDate',
        type:'date',
        trigger: 'click',
        done: function (value, date, endDate) {
            vm.ouinsuranceOrder.qxPayDate = value;
        }
    });

    //责任方
    layui.form.on('select(responsiblePartyFilter)', function (data) {
        vm.ouinsuranceOrder.responsibleParty = data.value;
    });

    //附件上传
    layui.upload.render({
        elem: '#addFile',
        url: baseURL + 'file/uploadInsuranceFile',
        data: {'path':'outinsuranceorder'},
        field:'files',
        auto:true,
        size: 50*1024*1024,
        accept: 'file', //普通文件
        acceptMime: '.pdf,.doc,.docx,.xls,.xlsx,.jpg,.png,.jpeg,.zip,.rar',
        exts: 'pdf|doc|docx|xls|xlsx|jpg|png|jpeg|zip|rar', //
        multiple: true,
        number:20,
        done: function (res) {
            RemoveLoading();
            if (res.code != '0') {
                layer.msg('上传失败', {icon: 5});
                vm.delFile(fileIdTmp);
                fileIdTmp = null;
                return false;
            }
            res.data.forEach(function (value) {
                var extIndex = value.resultFilePath.lastIndexOf('.');
                var ext = value.resultFilePath.slice(extIndex);
                var fileNameNotext = value.fileName;
                var regExt = /png|jpg|jpeg/;
                var fileType = regExt.test(ext) ? 1 : 0;
                fileIdTmp = vm.fileLst.length + '_' + uuid(60);
                var fileTmp = {
                    id: fileIdTmp,
                    operationId:sessionStorage.getItem("userId"),
                    operationName:sessionStorage.getItem("username"),
                    nameDesc: '出险附件',
                    nameAccessory: fileNameNotext,
                    nameFile: fileNameNotext,
                    nameExt: ext,
                    typeFile: fileType,
                    url: value.resultFilePath
                };
                vm.fileLst.push(fileTmp);
                vm.fileLstId = 'fileLstId_' + uuid(6);
            });
            fileIdTmp = null;
        },
        error: function () {
            RemoveLoading();
            layer.msg('上传失败', {icon: 5});
            vm.delFile(fileIdTmp);
            fileIdTmp = null;
        }
    });

    layui.form.on('submit(saveOrUpdate)', function(){
        vm.saveOrUpdate();
        return false;
    });

});

var vm = new Vue({
    el:'#rrapp',
    data:{
        showForm: false,
        carInforData:{},
        ouinsuranceOrder: {},
        fileLst:[],
        fileLstId: '0',
        //保险公司数据源
        compulsoryInsuranceList:[],
        //事故等级
        outLevelList:[],
        bpmChartDtoList:[],
        openFlow:false
    },
    updated: function(){
        layui.form.render();
    },
    created: function(){
        var _this = this;
        //初始化加载保险公司下拉列表
        $.ajaxSettings.async = false;
        //初始化加载保险公司下拉列表
        $.get(baseURL + "activity/bpmInitChart",{processKey:"carOutInsuranceApprove"}, function (r) {
            if (r.code == 0){
                _this.bpmChartDtoList = r.bpmInitChart;
                _this.openFlow = r.openFlow;
            }
        });
        $.get(baseURL + "insurancecompany/sysinsurancecompany/getInsuranceCompanyInforList", function(r){
            _this.compulsoryInsuranceList= r.compulsoryInsuranceList;
        });
        $.get(baseURL + "sys/dict/getInfoByType/"+"accidentLevel",function(r){
            _this.outLevelList= r.dict;
        });
        $.ajaxSettings.async = true;

    },
    methods: {
        cancel:function(){
            // parent.layer.closeAll();
            closeCurrent();
        },
        //选择车牌号
        selectCarNo:function(){
            var index = layer.open({
                title: "选择车牌号",
                type: 2,
                content: tabBaseURL + "modules/common/selectcar.html",
                success: function (layero, num) {
                    var iframe = window['layui-layer-iframe' + num];
                    iframe.vm.initType('outinsurance');
                },
                end: function(){
                    var carId=vm.carInforData.carId;
                    if(carId!=null && carId!='' && carId!=undefined){
                        vm.getCarBasicInforByCarNo(carId);
                    }
                }
            });
            layer.full(index);

        },
        //选择车架号
        selectVinNo:function(){
            var index = layer.open({
                title: "选择车架号",
                type: 2,
                content: tabBaseURL + "modules/common/selectcar.html",
                success: function (layero, num) {
                    var iframe = window['layui-layer-iframe' + num];
                    iframe.vm.initType('outinsurance');
                },
                end: function(){
                    var vinNo=vm.carInforData.vinNo;
                    if(vinNo!=null && vinNo!=''&& vinNo!=undefined){
                        vm.getCarBasicInforByVinNo(vinNo);
                    }
                }
            });
            layer.full(index);

        },
        //根据车牌号查询基本信息
        getCarBasicInforByCarNo:function (carId) {
            $.ajax({
                type: "POST",
                url: baseURL + "car/tcarbasic/getCarBasicInforByCarNo/"+carId,
                contentType: "application/json",
                data: {},
                success: function(r){
                    vm.ouinsuranceOrder=r.baseInfor;
                    console.log("打印r.baseInfor:"+r.baseInfor);
                }
            });
        },
        //根据车架号查询基本信息
        getCarBasicInforByVinNo:function (vinNo) {
            $.ajax({
                type: "POST",
                url: baseURL + "car/tcarbasic/getCarBasicInforByVinNo/"+vinNo,
                contentType: "application/json",
                data: {},
                success: function(r){
                    vm.ouinsuranceOrder=r.baseInfor;
                    console.log("打印r.baseInfor:"+r.baseInfor);
                }
            });
        },
        saveOrUpdate: function (event) {
            vm.ouinsuranceOrder.fileLst=vm.fileLst;
            vm.ouinsuranceOrder.carPlateNo=vm.ouinsuranceOrder.carNo;
            vm.ouinsuranceOrder.customer=vm.ouinsuranceOrder.customerName;
            vm.ouinsuranceOrder.carPurpose=vm.ouinsuranceOrder.rentTypeStr;
            vm.ouinsuranceOrder.depotId=vm.ouinsuranceOrder.carDepotId;
            vm.ouinsuranceOrder.depotName=vm.ouinsuranceOrder.carDepotName;

            var url ="outinsuranceorder/ouinsuranceorder/save";
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.ouinsuranceOrder),
                success: function(r){
                    if(r.code === 0){
                        alert('操作成功', function(index){
                            // parent.layer.closeAll();
                            // parent.vm.reload();
                            //页面关闭
                            closeCurrent();
                        });
                    }else{
                        alert(r.msg);
                    }
                }
            });
        },
        delFile: function (id) {
            for(var i = 0 ;i<vm.fileLst.length;i++) {
                if(vm.fileLst[i].id === id) {
                    vm.fileLst.splice(i,1);
                    i= i-1;
                }
            }
        },

    }
});


