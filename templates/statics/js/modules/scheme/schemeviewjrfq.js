$(function () {
    layui.config({
        base: '../../statics/common/'
    }).extend({
        soulTable: 'layui/soultable/ext/soulTable.slim'
    });
    layui.use(['form', 'layedit', 'laydate', 'element', 'table', 'soulTable'], function(){
        init(layui);
        layui.form.render();
    });
});

var vm = new Vue({
    el:'#rrapp',
    data:{
        detailsTabContentList: [ '金融分期方案', '操作记录',],
        detailsSupTabContentListActiveValue: null,
        detailsTabContentListActiveIndex : null,
        //主方案数据源
        scheme: {},
        //子方案数据源
        subScheme:[]

    },
    created: function(){
        var _this = this;
        var param = parent.layer.boxParams.boxParams;
        _this.scheme.schemeId = param;
    },
    updated: function(){
        layui.form.render();
    },
    methods: {
        detailsTabContentBindtap (index) {
            vm.detailsTabContentListActiveIndex = index;
            if (index == 0){
                vm.detailsSupTabContentListActiveIndex = index;
            }
            vm.detailsSupTabContentListActiveValue = vm.detailsTabContentList[index];
        },
        detailsSupTabContentBindtap (param, val) {
            vm.detailsSupTabContentListActiveIndex = param;
            vm.detailsSupTabContentListActiveValue = val;
        },
    }
});

function init(layui) {
    initTable(layui.table, layui.soulTable);
    initTableData(layui.table, layui.soulTable);
    initDate(layui.laydate);
    initEventListener(layui);
    initData();
}

function initData() {
    vm.detailsTabContentListActiveIndex = 0;
    vm.detailsSupTabContentListActiveIndex = 0;
    vm.detailsSupTabContentListActiveValue = vm.detailsTabContentList[0];
}

function initEventListener(layui) {
    initClick();
    initChecked(layui.form);
}

function initChecked(form) {

}

function initClick() {
    $("#closePage").on('click', function(){
        closePage();
    });
}


function initTableData(table, soulTable) {
    table.render({
        id: "gridid",
        elem: '#grid',
        data:vm.subScheme,
        cols: [[
            {field:'deptNames', title: '所属部门', minWidth:200, templet: function (d) {return isEmpty(d.deptNames);}},
            {field:'customerType', title: '客户类型', minWidth:200, templet: function (d) {
                    if(d.customerType == null || d.customerType == ''){
                        return "--";
                    }
                    if(d.customerType == 0){
                        return "全部";
                    }else if(d.customerType == 1){
                        return "个人";
                    }else {
                        return "企业";
                    }
                }},
            {field:'downPayment', title: '首付款/元', minWidth:200, templet: function (d) {return isEmpty(d.downPayment);}},
            {field:'monthlyRent', title: '月租金/元', minWidth:200, templet: function (d) {return isEmpty(d.monthlyRent);}},
            {field:'tenancy', title: '租期', minWidth:200, templet: function (d) {
                    if(d.tenancy!=null && d.tenancy!=''){
                        return  d.tenancy +"期";
                    }else{
                        return isEmpty(d.tenancy);
                    }

                }},
            {field:'balance', title: '尾款/元', minWidth:200, templet: function (d) {return isEmpty(d.balance);}},
            {field:'chlNames', title: '所属渠道', minWidth:200, templet: function (d) {return isEmpty(d.chlNames);}},
            {field:'chlMonthlyRent', title: '月租金渠道价/元', minWidth:200, templet: function (d) {return isEmpty(d.chlMonthlyRent);}},
            {field:'financeCompanyName', title: '金融公司', minWidth:200, templet: function (d) {return isEmpty(d.financeCompanyName);}},
            {field:'chlRebate', title: '渠道商返利/元', minWidth:200, templet: function (d) {return isEmpty(d.chlRebate);}},
            {field:'isEnable', title: '启用停用', minWidth:200, templet: function (d) {
                    if(d.isEnable==1){
                        return "启用";
                    }else if(d.isEnable==2){
                        return "停用";
                    }else {
                        return isEmpty(d.isEnable);
                    }

                }},
            {field:'createTime', title: '创建时间', minWidth:200, templet: function (d) {
                    if(d.createTime!=null && d.createTime!=''){
                        return dateFormat(d.createTime);
                    }else {
                        return isEmpty(d.createTime);
                    }
                }},

        ]],
        page: true,
        loading: true,
        limits: [20, 50, 100, 200],
        limit: 20,
        autoColumnWidth: {
            init: false
        },
        done: function(res, curr, count){
            soulTable.render(this);
            $('div[lay-id="gridid"]>div[class="layui-table-box"]>div>table').addClass('table-empty-left');
            $(".layui-table-main tr").each(function (index, val) {
                $($(".layui-table-fixed-l .layui-table-body tbody tr")[index]).height($(val).height());
                $($(".layui-table-fixed-r .layui-table-body tbody tr")[index]).height($(val).height());
            });
        }
    });

    initTableEvent(table);
    // initTableEditListner(table);
}

function initTable(table, soulTable) {
    table.render({
        id: "operationlogid",
        elem: '#operationlog',
        // defaultToolbar: ['filter'],
        url: baseURL + 'sys/log/operationLogLst',
        where: {
            businessNo: vm.scheme.schemeId,
            auditType: 45
        },
        cols: [[
            {field:'operatorName', title: '操作人', minWidth:200, templet: function (d) {return isEmpty(d.operatorName);}},
            {field:'memo', title: '操作内容', minWidth:200, templet: function (d) {return isEmpty(d.memo);}},
            {field:'timeCreate', title: '操作时间', minWidth:200, templet: function (d) {return isEmpty(d.timeCreate);}},
        ]],
        page: true,
        loading: true,
        limits: [20, 50, 100, 200],
        limit: 20,
        autoColumnWidth: {
            init: false
        },
        done: function(res, curr, count){
            soulTable.render(this);
        }
    });

    initTableEvent(table);
    initTableEditListner(table);
}

function initTableEditListner(table) {

}

function initTableEvent(table) {

}

function initDate(laydate) {

}

function closePage() {
    parent.vm.isClose = true;
    var index = parent.layer.getFrameIndex(window.name);
    parent.vm.reload();
    parent.layer.close(index);
}

function sendData(schemeEntityDTO) {
    vm.scheme = Object.assign({}, vm.scheme, {
        schemeName: schemeEntityDTO.schemeName,
        schemeId :schemeEntityDTO.schemeId,
        schemeType :schemeEntityDTO.schemeType,
        brandName :schemeEntityDTO.brandName,
        seriesName :schemeEntityDTO.seriesName,
        modelName :schemeEntityDTO.modelName,
        sort :schemeEntityDTO.sort,
        isPopularCarRental:schemeEntityDTO.isPopularCarRental,
        isPopularCarRentalShow:schemeEntityDTO.isPopularCarRentalShow
    })
    //给数据表格赋值
    vm.subScheme=schemeEntityDTO.subSchemeEntityList;
    layui.table.reload('gridid', {
        data: vm.subScheme
    });
    layui.form.render();
}