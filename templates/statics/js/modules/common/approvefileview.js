$(function () {
    layui.config({
        base: '../../statics/common/'
    }).extend({
        soulTable: 'layui/soultable/ext/soulTable.slim'
    });
    layui.use(['form', 'layer','layedit', 'laydate', 'element', 'table', 'soulTable'], function(){
        init(layui);
        layui.form.render();
    });
});
var viewer;
var vm = new Vue({
    el:'#rrapp',
    data:{
        fileList:[]
    },
    created: function(){
        var _this = this;
        var param = parent.layer.boxParams.boxParams;
        _this.fileList = param.data;
    },
    updated: function(){
        layui.form.render();
    },
    methods: {
        downDoc:function(data){
            var uri = baseURL + 'file/download?uri=' + data.url + "&fileName=" + data.nameFile;
            window.location.href = uri;
        },
        showDoc: function (url) {
            if (viewer != null){
                viewer.close();
                viewer = null;
            }
            viewer = new PhotoViewer([
                {
                    src: fileURL+url,
                    title: '审批资料'
                }
            ], {
                appendTo:'body',
                zIndex:99891018
            });
        },

    },

});

function init(layui) {
    initEventListener(layui);
    initTable(layui.table, layui.soulTable);

}

function initEventListener(layui) {
    initClick();
}

function initTable(table, soulTable){




}


function initClick() {
    $("#closePage").on('click', function(){
        closePage();
    });
}

function closePage() {
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}
