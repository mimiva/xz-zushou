$(function () {
    layui.config({
        base: '../../statics/common/'
    }).extend({
        soulTable: 'layui/soultable/ext/soulTable.slim'
    });
    layui.use(['form', 'layedit', 'laydate', 'element', 'table', 'soulTable'], function () {
        init(layui);
        layui.form.render();
    });
});

var vm = new Vue({
    el: '#rrapp',
    data: {
        q: {
            serviceName: null,
            serviceType: null,
        },


    },
    created: function () {


    },
    mounted: function (){


    },
    updated: function () {
        layui.form.render();
    },
    methods: {
        query: function () {
            vm.reload();
        },
        reset: function () {
            vm.q.serviceName=null;
            vm.q.serviceType=null;
        },
        view: function (couponId) {
            $.get(baseURL + "serviceConfig/info?id="+couponId, function (r) {
                var param = {
                    data: r.data
                };
                var index = layer.open({
                    title: "查看",
                    type: 2,
                    boxParams: param,
                    content: tabBaseURL + "modules/serviceConfig/configview.html",
                    end: function () {
                        layer.close(index);
                    }
                });
                layer.full(index);
            });
        },
        add: function () {
            var param = {
                data: {}
            };
            var index = layer.open({
                title: "新增",
                type: 2,
                boxParams: param,
                content: tabBaseURL + "modules/serviceConfig/configedit.html",
                end: function () {
                    layer.close(index);
                }
            });
            layer.full(index);
        },
        update: function (couponId) {
            $.get(baseURL +"serviceConfig/info?id="+couponId, function (r) {
                var param = {
                    data: r.data
                };
                var index = layer.open({
                    title: "修改",
                    type: 2,
                    boxParams: param,
                    content: tabBaseURL + "modules/serviceConfig/configedit.html",
                    end: function () {
                        layer.close(index);
                    }
                });
                layer.full(index);
            });


        },

        del: function (couponId) {
            confirm('确定要删除此配置项吗？', function () {
                $.ajax({
                    type: "POST",
                    url: baseURL + "/serviceConfig/delete?id=" + couponId,
                    data: {},
                    success: function (r) {
                        if (r.code == 0) {
                            alert('操作成功', function (index) {
                                vm.reload();
                            });
                        } else {
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        reload: function (event) {
            layui.table.reload('grid', {
                page: {
                    curr: 1
                },
                where: JSON.parse(JSON.stringify(vm.q))
            });
        }
    }

});

function init(layui) {
    initTable(layui.table, layui.soulTable);
    initEventListener(layui);
    initData();
    initDate(layui.laydate);
}


function initData() {
    //初始化查询数据字典-设备生产商

}
function initDate(laydate) {
    laydate.render({
        elem: '#q_startTime' //指定元素
        ,zIndex: 99999999,
        trigger: 'click',
        range: true,
        done: function(value, date, endDate){
            vm.q.startTime = value;
        }
    });

}

function initEventListener(layui) {
    initClick();
    initChecked(layui.form);
}

function initChecked(form) {
    form.on('select(serviceType)', function (data) {
        vm.q.serviceType = data.value;
    });


}

function initClick() {
    $(".delBatch").click(function () {
        var deviceIds = vm.selectedRows();
        if (deviceIds == null) {
            return;
        }
        vm.del(deviceIds);
    });
}

function initTable(table, soulTable) {
    table.render({
        id: "grid",
        elem: '#grid',
        url: baseURL +'/serviceConfig/list',
        // where: JSON.parse(JSON.stringify(vm.q)),
        cols: [[
            //  {type:'checkbox'},
            {title: '操作', width: 200, templet: '#barTpl', fixed: "left", align: "center"},
            {
                field: 'serviceName', title: '费用名称', minWidth: 200, templet: function (d) {
                    return isEmpty(d.serviceName);
                }
            },
            {
                field: 'serviceType', title: '费用类型', minWidth: 200, templet: function (d) {
                    var serviceType = d.serviceType;
                    if (serviceType == 1) {
                        return "可退金额";
                    } else if (serviceType == 2) {
                        return "违章费用";
                    } else if (serviceType == 3) {
                        return "保险费用";
                    }else if (serviceType == 4) {
                        return "扣款费用";
                    }else if (serviceType == 5) {
                        return "银行信息";
                    }else if (serviceType == 6) {
                        return "退换车说明";
                    }else {
                        return "--"
                    }
                }
            },
            {
                field: 'serviceProperty', title: '费用性质', minWidth: 200, templet: function (d) {
                    var serviceProperty = d.serviceProperty;
                    if (serviceProperty == 0) {
                        return "正常";
                    } else if (serviceProperty == 1) {
                        return "收入";
                    } else if (serviceProperty == 2) {
                        return "支出";
                    }else {
                        return "--"
                    }
                }
            },
            {
                field: 'serviceField', title: '字段值', minWidth: 200, templet: function (d) {
                    return isEmpty(d.serviceField);
                }
            },
            {
                field: 'suitRentType', title: '适用租赁类型', minWidth: 200, templet: function (d) {
                    var suitRentType = d.suitRentType;
                    if (suitRentType == 1) {
                        return "经租";
                    } else if (suitRentType == 2) {
                        return "以租代购";
                    } else if (suitRentType == 6) {
                        return "直购";
                    }else {
                        return "--"
                    }
                }
            },
            {
                field: 'suitType', title: '适用业务类型', minWidth: 200, templet: function (d) {
                    var suitType = d.suitType;
                    if (suitType == 1) {
                        return "退车";
                    } else if (suitType == 2) {
                        return "换车";
                    } else if (suitType == 3) {
                        return "备用车";
                    }else if (suitType == 4) {
                        return "退换车";
                    }else {
                        return "--"
                    }
                }
            },
            {
                field: 'createTime', title: '创建时间', minWidth: 200, templet: function (d) {
                    return isEmpty(d.createTime);
                }
            },
            {
                field: 'updateTime', title: '更新时间', minWidth: 200, templet: function (d) {
                    return isEmpty(d.updateTime);
                }
            }
        ]],
        page: true,
        loading: true,
        limits: [10,20, 50, 100],
        limit: 10,
        autoColumnWidth: {
            init: false
        },
        done: function (res, curr, count) {
            soulTable.render(this);
            $('div[lay-id="gridid"]>div[class="layui-table-box"]>div>table').addClass('table-empty-left');
            $(".layui-table-main tr").each(function (index, val) {
                $($(".layui-table-fixed-l .layui-table-body tbody tr")[index]).height($(val).height());
                $($(".layui-table-fixed-r .layui-table-body tbody tr")[index]).height($(val).height());
            });
        }
    });

    initTableEvent(table);

}


function initTableEvent(table) {
    table.on('tool(grid)', function (obj) {
        var layEvent = obj.event,
            data = obj.data;
        if (layEvent === 'edit') {
            vm.update(data.id);
        } else if (layEvent === 'view') {
            vm.view(data.id);
        }else if (layEvent === 'del') {
            vm.del(data.id);
        }

    });
}


