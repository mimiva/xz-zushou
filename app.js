const express = require('express')
const { createProxyMiddleware } = require('http-proxy-middleware')
const cliColor = require('cli-color')
const settings = require('./settings')

const app = express()

app.get('/', (req, res) => {
    res.redirect('index.html')
})

app.use(express.static('templates'))

app.use(createProxyMiddleware(settings.ProxyPath, {
    target: settings.ProxyHost, // target host
    // changeOrigin: true, // needed for virtual hosted sites
}))

app.listen(80, () => {
    console.log(cliColor.blueBright('服务启动成功: http://127.0.0.1'))
})
