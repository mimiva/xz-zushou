# 行知车辆租售管理平台

## 安装

```
$ npm i 
```

## 启动

```
$ npm start
```

## 部署

nginx静态资源目录指向templates

本地开发时给nginx添加使用
```shell
upstream api_service {
  # 后端接口地址
  server 127.0.0.1:8065 weight=1;
}
server {
    listen       8889;
    server_name  127.0.0.1;

    root         E://project//xz-zushou-ui//templates;
    # 对接定位接口
    location ^~ /xz-admin-api/ {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;		 
        proxy_pass http://api_service/xz-admin-api/;
        client_max_body_size 100m;		  
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }
}
```

## 在线文档参考
- [zTree](https://treejs.cn/v3/api.php)
- [jquery](https://www.runoob.com/jquery/jquery-tutorial.html)
- [layui](https://layui.gitee.io/v2/docs/)
- [vuejs v2](https://v2.cn.vuejs.org/v2/guide/installation.html)
- [xm-select](https://maplemei.gitee.io/xm-select/#/basic/prop) `下拉框多选`